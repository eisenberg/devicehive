package com.devicehive.model;

import com.devicehive.exceptions.HiveException;
import com.devicehive.json.GsonFactory;
import com.devicehive.json.strategies.JsonPolicyDef;
import com.google.gson.*;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static com.devicehive.json.strategies.JsonPolicyDef.Policy.*;

@Entity
@NamedQueries({
        @NamedQuery(name = "AccessKeyPermission.deleteByAccessKey", query = "delete from AccessKeyPermission akp where akp.accessKey = :accessKey")
})
@Table(name = "access_key_permission")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class AccessKeyPermission implements HiveEntity {

    private static final long serialVersionUID = 728578066176830685L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "access_key_id")
    @NotNull
    private AccessKey accessKey;
    /*@Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "jsonString", column = @Column(name = "domains"))
    })
    @JsonPolicyDef({ACCESS_KEY_LISTED, ACCESS_KEY_PUBLISHED, OAUTH_GRANT_LISTED_ADMIN, OAUTH_GRANT_LISTED})
    private JsonStringWrapper domains;*/
    @Type(type="text")
    @Column(name = "domains")
    @JsonPolicyDef({ACCESS_KEY_LISTED, ACCESS_KEY_PUBLISHED, OAUTH_GRANT_LISTED_ADMIN, OAUTH_GRANT_LISTED})
    private String domains;
    /*@Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "jsonString", column = @Column(name = "subnets"))
    })
    @JsonPolicyDef({ACCESS_KEY_LISTED, ACCESS_KEY_PUBLISHED, OAUTH_GRANT_LISTED_ADMIN, OAUTH_GRANT_LISTED})
    private JsonStringWrapper subnets;*/
    @Type(type="text")
    @Column(name = "subnets")
    @JsonPolicyDef({ACCESS_KEY_LISTED, ACCESS_KEY_PUBLISHED, OAUTH_GRANT_LISTED_ADMIN, OAUTH_GRANT_LISTED})
    private String subnets;
    /*@Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "jsonString", column = @Column(name = "pactions"))
    })
    @JsonPolicyDef({ACCESS_KEY_LISTED, ACCESS_KEY_PUBLISHED, OAUTH_GRANT_LISTED_ADMIN, OAUTH_GRANT_LISTED})
    private JsonStringWrapper pactions;*/
    @Type(type="text")
    @Column(name = "pactions")
    @JsonPolicyDef({ACCESS_KEY_LISTED, ACCESS_KEY_PUBLISHED, OAUTH_GRANT_LISTED_ADMIN, OAUTH_GRANT_LISTED})
    private String pactions;
    /*@Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "jsonString", column = @Column(name = "network_ids"))
    })
    @JsonPolicyDef({ACCESS_KEY_LISTED, ACCESS_KEY_PUBLISHED, OAUTH_GRANT_LISTED_ADMIN, OAUTH_GRANT_LISTED})
    private JsonStringWrapper networkIds;*/
    @Type(type="text")
    @Column(name = "network_ids")
    @JsonPolicyDef({ACCESS_KEY_LISTED, ACCESS_KEY_PUBLISHED, OAUTH_GRANT_LISTED_ADMIN, OAUTH_GRANT_LISTED})
    private String networkIds;
    /*@Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "jsonString", column = @Column(name = "device_guids"))
    })
    @JsonPolicyDef({ACCESS_KEY_LISTED, ACCESS_KEY_PUBLISHED, OAUTH_GRANT_LISTED_ADMIN, OAUTH_GRANT_LISTED})
    private JsonStringWrapper deviceGuids;*/
    @Type(type="text")
    @Column(name = "device_guids")
    @JsonPolicyDef({ACCESS_KEY_LISTED, ACCESS_KEY_PUBLISHED, OAUTH_GRANT_LISTED_ADMIN, OAUTH_GRANT_LISTED})
    private String deviceGuids;
    @Version
    @Column(name = "entity_version")
    private long entityVersion;

    public long getEntityVersion() {
        return entityVersion;
    }

    public void setEntityVersion(long entityVersion) {
        this.entityVersion = entityVersion;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AccessKey getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(AccessKey accessKey) {
        this.accessKey = accessKey;
    }

//    public JsonStringWrapper getDomains() {
//        return domains;
//    }
    public String getDomains() {
        return domains;
    }

//    public void setDomainArray(String... domains) {
//        Gson gson = GsonFactory.createGson();
//        this.domains = new JsonStringWrapper(gson.toJsonTree(domains).toString());
//    }
    public void setDomainArray(String... domains) {
        Gson gson = GsonFactory.createGson();
        this.domains = gson.toJsonTree(domains).toString();
    }

//    public void setDomains(JsonStringWrapper domains) {
//        this.domains = domains;
//    }
    public void setDomains(String domains) {
        this.domains = domains;
    }

    public Set<String> getDomainsAsSet() {
        return getJsonAsSet(domains);
    }

//    public Set<Subnet> getSubnetsAsSet() {
//        if (subnets == null) {
//            return null;
//        }
//        JsonParser parser = new JsonParser();
//        JsonElement elem = parser.parse(subnets.getJsonString());
//        if (elem instanceof JsonNull) {
//            return null;
//        }
//        if (elem instanceof JsonArray) {
//            JsonArray json = (JsonArray) elem;
//            Set<Subnet> result = new HashSet<>(json.size());
//            for (JsonElement current : json) {
//                if (!current.isJsonNull()) {
//                    result.add(new Subnet(current.getAsString()));
//                } else {
//                    result.add(null);
//                }
//            }
//            return result;
//        }
//        throw new HiveException("JSON array expected!", HttpServletResponse.SC_BAD_REQUEST);
//    }
    public Set<Subnet> getSubnetsAsSet() {
        if (subnets == null) {
            return null;
        }
        JsonParser parser = new JsonParser();
        JsonElement elem = parser.parse(subnets);
        if (elem instanceof JsonNull) {
            return null;
        }
        if (elem instanceof JsonArray) {
            JsonArray json = (JsonArray) elem;
            Set<Subnet> result = new HashSet<>(json.size());
            for (JsonElement current : json) {
                if (!current.isJsonNull()) {
                    result.add(new Subnet(current.getAsString()));
                } else {
                    result.add(null);
                }
            }
            return result;
        }
        throw new HiveException("JSON array expected!", HttpServletResponse.SC_BAD_REQUEST);
    }


    public Set<String> getActionsAsSet() {
        return getJsonAsSet(pactions);
    }

    public Set<String> getDeviceGuidsAsSet() {
        return getJsonAsSet(deviceGuids);
    }
    private Set<String> getJsonAsSet(String wrapper) {
        if (wrapper == null) {
            return null;
        }
        JsonParser parser = new JsonParser();
        JsonElement elem = parser.parse(wrapper);
        if (elem instanceof JsonNull) {
            return null;
        }

        if (elem instanceof JsonArray) {
            JsonArray json = (JsonArray) elem;
            Set<String> result = new HashSet<>(json.size());
            for (JsonElement current : json) {
                result.add(current.getAsString());
            }
            return result;
        }
        throw new HiveException("JSON array expected!", HttpServletResponse.SC_BAD_REQUEST);
    }
//    public Set<Long> getNetworkIdsAsSet() {
//        if (networkIds == null) {
//            return null;
//        }
//        JsonParser parser = new JsonParser();
//        JsonElement elem = parser.parse(networkIds.getJsonString());
//        if (elem instanceof JsonNull) {
//            return null;
//        }
//        if (elem instanceof JsonArray) {
//            JsonArray json = (JsonArray) elem;
//            Set<Long> result = new HashSet<>(json.size());
//            for (JsonElement current : json) {
//                result.add(current.getAsLong());
//            }
//            return result;
//        }
//        throw new HiveException("JSON array expected!", HttpServletResponse.SC_BAD_REQUEST);
//    }
    public Set<Long> getNetworkIdsAsSet() {
        if (networkIds == null) {
            return null;
        }
        JsonParser parser = new JsonParser();
        JsonElement elem = parser.parse(networkIds);
        if (elem instanceof JsonNull) {
            return null;
        }
        if (elem instanceof JsonArray) {
            JsonArray json = (JsonArray) elem;
            Set<Long> result = new HashSet<>(json.size());
            for (JsonElement current : json) {
                result.add(current.getAsLong());
            }
            return result;
        }
        throw new HiveException("JSON array expected!", HttpServletResponse.SC_BAD_REQUEST);
    }

    private Set<String> getJsonAsSet(JsonStringWrapper wrapper) {
        if (wrapper == null) {
            return null;
        }
        JsonParser parser = new JsonParser();
        JsonElement elem = parser.parse(wrapper.getJsonString());
        if (elem instanceof JsonNull) {
            return null;
        }

        if (elem instanceof JsonArray) {
            JsonArray json = (JsonArray) elem;
            Set<String> result = new HashSet<>(json.size());
            for (JsonElement current : json) {
                result.add(current.getAsString());
            }
            return result;
        }
        throw new HiveException("JSON array expected!", HttpServletResponse.SC_BAD_REQUEST);
    }

//    public JsonStringWrapper getSubnets() {
//        return subnets;
//    }
    public String getSubnets() {
        return subnets;
    }

//    public void setSubnets(JsonStringWrapper subnets) {
//        this.subnets = subnets;
//    }
    public void setSubnets(String subnets) {
        this.subnets = subnets;
    }

//    public void setSubnetsArray(String... subnets) {
//        Gson gson = GsonFactory.createGson();
//        this.subnets = new JsonStringWrapper(gson.toJsonTree(subnets).toString());
//    }
    public void setSubnetsArray(String... subnets) {
        Gson gson = GsonFactory.createGson();
        this.subnets = gson.toJsonTree(subnets).toString();
    }

//
//    public JsonStringWrapper getPactions() {
//        return pactions;
//    }
    public String getPactions() {
        return pactions;
    }

//    public void setPactions(JsonStringWrapper actions) {
//        this.pactions = actions;
//    }
    public void setPactions(String actions) {
        this.pactions = actions;
    }

//    public void setActionsArray(String... actions) {
//        Gson gson = GsonFactory.createGson();
//        this.pactions = new JsonStringWrapper(gson.toJsonTree(actions).toString());
//    }
    public void setActionsArray(String... actions) {
        Gson gson = GsonFactory.createGson();
        this.pactions = gson.toJsonTree(actions).toString();
    }

//    public JsonStringWrapper getNetworkIds() {
//        return networkIds;
//    }
    public String getNetworkIds() {
        return networkIds;
    }

//    public void setNetworkIds(JsonStringWrapper networkIds) {
//        this.networkIds = networkIds;
//    }
    public void setNetworkIds(String networkIds) {
        this.networkIds = networkIds;
    }


//    public void setNetworkIdsCollection(Collection<Long> actions) {
//        Gson gson = GsonFactory.createGson();
//        this.networkIds = new JsonStringWrapper(gson.toJsonTree(actions).toString());
//    }
    public void setNetworkIdsCollection(Collection<Long> actions) {
        Gson gson = GsonFactory.createGson();
        this.networkIds = gson.toJsonTree(actions).toString();
    }
    
/*
    public JsonStringWrapper getDeviceGuids() {
        return deviceGuids;
    }

    public void setDeviceGuids(JsonStringWrapper deviceGuids) {
        this.deviceGuids = deviceGuids;
    }

    public void setDeviceGuidsCollection(Collection<String> deviceGuids) {
        Gson gson = GsonFactory.createGson();
        this.deviceGuids = new JsonStringWrapper(gson.toJsonTree(deviceGuids).toString());
    }*/
    public String getDeviceGuids() {
        return deviceGuids;
    }

    public void setDeviceGuids(String deviceGuids) {
        this.deviceGuids = deviceGuids;
    }

    public void setDeviceGuidsCollection(Collection<String> deviceGuids) {
        Gson gson = GsonFactory.createGson();
        this.deviceGuids =gson.toJsonTree(deviceGuids).toString();
    }
}
