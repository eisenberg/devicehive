package com.devicehive.model.response;

import static com.devicehive.json.strategies.JsonPolicyDef.Policy.USERS_LISTED;
import static com.devicehive.json.strategies.JsonPolicyDef.Policy.USER_PUBLISHED;
import static com.devicehive.json.strategies.JsonPolicyDef.Policy.DEVICE_NOT_INSERT;

import com.devicehive.json.strategies.JsonPolicyDef;
import com.devicehive.model.Device;
import com.devicehive.model.HiveEntity;
import com.google.common.collect.ImmutableSet;
import com.google.gson.annotations.SerializedName;

public class BatchInsertResponse implements HiveEntity {

	private static final long serialVersionUID = 7945751651615680861L;

	@SerializedName("code")
	@JsonPolicyDef({ USER_PUBLISHED, USERS_LISTED,DEVICE_NOT_INSERT})
	private int code;

	@SerializedName("info")
	@JsonPolicyDef({ USER_PUBLISHED, USERS_LISTED,DEVICE_NOT_INSERT })
	private ImmutableSet<String> info;
	
	
	public BatchInsertResponse(int code,ImmutableSet<String> info){
		this.code = code;
		this.info = info;
	}

}
