package com.devicehive.model.response;

import static com.devicehive.json.strategies.JsonPolicyDef.Policy.USERS_LISTED;
import static com.devicehive.json.strategies.JsonPolicyDef.Policy.USER_PUBLISHED;

import com.devicehive.json.strategies.JsonPolicyDef;
import com.devicehive.model.HiveEntity;
import com.devicehive.model.enums.UserRole;
import com.google.gson.annotations.SerializedName;

public class StatusResponse implements HiveEntity {

	private static final long serialVersionUID = 7945751651615680861L;

	@SerializedName("code")
	@JsonPolicyDef({ USER_PUBLISHED, USERS_LISTED })
	private int code;

	@SerializedName("info")
	@JsonPolicyDef({ USER_PUBLISHED, USERS_LISTED })
	private String info;
	
	
	public StatusResponse(int code,String info){
		this.code = code;
		this.info = info;
	}

}
