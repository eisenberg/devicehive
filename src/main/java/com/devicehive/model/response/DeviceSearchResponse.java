package com.devicehive.model.response;

import static com.devicehive.json.strategies.JsonPolicyDef.Policy.DEVICE_SEARCH_NUM_ONLY;
import static com.devicehive.json.strategies.JsonPolicyDef.Policy.DEVICE_SEARCH_RETURN;

import com.devicehive.json.strategies.JsonPolicyDef;
import com.devicehive.model.Device;
import com.devicehive.model.HiveEntity;
import com.devicehive.model.response.DeviceSearchResponse.ReturnSearchDevice;
import com.google.common.collect.ImmutableSet;
import com.google.gson.annotations.SerializedName;

public class DeviceSearchResponse implements HiveEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6693905037994749688L;
	@JsonPolicyDef({ DEVICE_SEARCH_NUM_ONLY,DEVICE_SEARCH_RETURN })
	@SerializedName("code")
	private Integer code;
	@JsonPolicyDef({ DEVICE_SEARCH_NUM_ONLY,DEVICE_SEARCH_RETURN })
	@SerializedName("info")
	private ReturnSearchDevice info;
	
	public DeviceSearchResponse(int i, ReturnSearchDevice info2) {
		code = i;
		info = info2;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public ReturnSearchDevice getInfo() {
		return info;
	}

	public void setInfo(ReturnSearchDevice info) {
		this.info = info;
	}

	public static class ReturnSearchDevice{
		@JsonPolicyDef({ DEVICE_SEARCH_NUM_ONLY,DEVICE_SEARCH_RETURN })
		@SerializedName("total")
		private int total;
		@JsonPolicyDef({DEVICE_SEARCH_RETURN })
		@SerializedName("devices")
		private ImmutableSet<Device> devices;
		public int getTotal() {
			return total;
		}
		public void setTotal(int total) {
			this.total = total;
		}
		public ImmutableSet<Device> getDevices() {
			return devices;
		}
		public void setDevices(ImmutableSet<Device> devices) {
			this.devices = devices;
		} 
		
		
	}

}
