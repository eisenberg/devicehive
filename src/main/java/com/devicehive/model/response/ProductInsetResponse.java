package com.devicehive.model.response;

import com.devicehive.json.strategies.JsonPolicyDef;
import com.devicehive.json.strategies.JsonPolicyDef.Policy;
import com.devicehive.model.HiveEntity;
import com.devicehive.model.Product;
import com.google.gson.annotations.SerializedName;

public class ProductInsetResponse implements HiveEntity {
	@SerializedName("code")
	@JsonPolicyDef({Policy.PRODUCT_PUBLISH})
	private int code;

	@SerializedName("info")
	@JsonPolicyDef({Policy.PRODUCT_PUBLISH })
	private Product info;

public ProductInsetResponse(int code,Product info){
	this.code = code;
	this.info = info;
}

}
