package com.devicehive.model.tenant;

import static com.devicehive.json.strategies.JsonPolicyDef.Policy.DEVICE_SUBMITTED;

import java.io.Serializable;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import com.devicehive.configuration.Constants;
import com.devicehive.exceptions.CommonException;
import com.devicehive.json.strategies.JsonPolicyDef;
import com.devicehive.model.Device;
import com.google.gson.annotations.SerializedName;

public class TenantDevice implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SerializedName("mac")
	@JsonPolicyDef({ DEVICE_SUBMITTED })
	private String mac;

	@SerializedName("deviceId")
	@JsonPolicyDef({ DEVICE_SUBMITTED })
	private String deviceID;

	@SerializedName("deviceName")
	@JsonPolicyDef({ DEVICE_SUBMITTED })
	private String deviceName;

	@SerializedName("networkId")
	@JsonPolicyDef({ DEVICE_SUBMITTED })
	private String networkId;

	@SerializedName("deviceClass")
	@JsonPolicyDef({ DEVICE_SUBMITTED })
	private String deviceClass;

	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}

	public String getDeviceClass() {
		return deviceClass;
	}

	public void setDeviceClass(String deviceClass) {
		this.deviceClass = deviceClass;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public Device convertTo() {
		Device device = new Device();
		if (StringUtils.isNotBlank(deviceID)) {
			device.setGuid(deviceID);
		} else {
			return null;
		}
		if (StringUtils.isNotBlank(mac)) {
			device.setMac(mac);
		} else {
			return null;
		}
		device.setName(deviceName);
		return device;
	}

	@Override
	public String toString() {
		return "TenantDevice [mac=" + mac + ", deviceID=" + deviceID + ", deviceName=" + deviceName + ", networkId="
				+ networkId + ", deviceClass=" + deviceClass + "]";
	}

}
