package com.devicehive.model.tenant;

import static com.devicehive.json.strategies.JsonPolicyDef.Policy.DEVICE_SUBMITTED;

import com.devicehive.json.strategies.JsonPolicyDef;
import com.devicehive.model.HiveEntity;
import com.google.gson.annotations.SerializedName;

public class TenantDeviceList implements HiveEntity {
	@SerializedName("product")
	@JsonPolicyDef({DEVICE_SUBMITTED})
	private String product;
	@SerializedName("tenant")
	@JsonPolicyDef({DEVICE_SUBMITTED})
	private String tenant;
	@SerializedName("devices")
	@JsonPolicyDef({DEVICE_SUBMITTED})
	private TenantDevice[] devices;
	
	
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getTenant() {
		return tenant;
	}
	public void setTenant(String tenant) {
		this.tenant = tenant;
	}
	public TenantDevice[] getDevices() {
		return devices;
	}
	public void setDevices(TenantDevice[] devices) {
		this.devices = devices;
	}
	
	
	
}
