package com.devicehive.model;

import com.devicehive.json.strategies.JsonPolicyDef;
import com.google.gson.annotations.SerializedName;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.devicehive.json.strategies.JsonPolicyDef.Policy.*;

/**
 * TODO JavaDoc
 */

@Entity
@Table(name = "device")
@NamedQueries({ @NamedQuery(name = "Device.findByGuid", query = "select d from Device d where d.guid = :guid"),
		@NamedQuery(name = "Device.findByUUID", query = "select d from Device d " + "left join fetch d.network "
				+ "left join fetch d.deviceClass dc " + "where d.guid = :guid"),
		@NamedQuery(name = "Device.deleteByUUID", query = "delete from Device d where d.guid = :guid"),
		@NamedQuery(name = "Device.findByVendorId", query = "select d from Device d where d.vendordeviceid = :vendordeviceid"),
		@NamedQuery(name = "Device.findNetworkByVendorId", query = "select d from Device d "
				+ "left join fetch d.network " + "where d.vendordeviceid = :vendordeviceid") })
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Device implements HiveEntity {

	public static final String NETWORK_COLUMN = "network";
	public static final String GUID_COLUMN = "guid";

	private static final long serialVersionUID = 2959997451631843298L;
	@Id
	@SerializedName("sid") // overwork for "declares multiple JSON fields"
							// exception
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@SerializedName("id")
	@Column
	@NotNull(message = "guid field cannot be null.")
	@Size(min = 1, max = 48, message = "Field cannot be empty. The length of guid should not be more than 48 symbols.")
	@JsonPolicyDef({ DEVICE_PUBLISHED, DEVICE_PUBLISHED_DEVICE_AUTH, NETWORK_PUBLISHED, DEVICE_NOT_INSERT,DEVICE_SEARCH_RETURN})
	private String guid;
	@SerializedName("name")
	@Column
	@NotNull(message = "name field cannot be null.")
	@Size(min = 1, max = 128, message = "Field cannot be empty. The length of name should not be more than 128 "
			+ "symbols.")
	@JsonPolicyDef({ DEVICE_PUBLISHED, DEVICE_PUBLISHED_DEVICE_AUTH, DEVICE_SUBMITTED, NETWORK_PUBLISHED })
	private String name;
	@SerializedName("status")
	@Column
	@Size(min = 1, max = 128, message = "Field cannot be empty. The length of status should not be more than 128 symbols.")
	@JsonPolicyDef({ DEVICE_PUBLISHED, DEVICE_PUBLISHED_DEVICE_AUTH, DEVICE_SUBMITTED, NETWORK_PUBLISHED })
	private String status;
	/*
	 * @SerializedName("data")
	 * 
	 * @Embedded
	 * 
	 * @AttributeOverrides({
	 * 
	 * @AttributeOverride(name = "jsonString", column = @Column(name = "data"))
	 * })
	 * 
	 * @JsonPolicyDef({DEVICE_PUBLISHED, DEVICE_PUBLISHED_DEVICE_AUTH,
	 * DEVICE_SUBMITTED, NETWORK_PUBLISHED}) private JsonStringWrapper data;
	 */
	@Type(type = "text")
	@Column(name = "data")
	@SerializedName("data")
	@JsonPolicyDef({ DEVICE_PUBLISHED, DEVICE_PUBLISHED_DEVICE_AUTH, DEVICE_SUBMITTED, NETWORK_PUBLISHED })
	private String data;
	@SerializedName("network")
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "network_id")
	@JsonPolicyDef({ DEVICE_PUBLISHED, DEVICE_PUBLISHED_DEVICE_AUTH, DEVICE_SUBMITTED })
	private Network network;
	@SerializedName("deviceClass")
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE/*PERSIST*/)
	@JoinColumn(name = "device_class_id")
	// @NotNull(message = "deviceClass field cannot be null.")
	@JsonPolicyDef({ DEVICE_PUBLISHED, DEVICE_PUBLISHED_DEVICE_AUTH, DEVICE_SUBMITTED, NETWORK_PUBLISHED })
	private DeviceClass deviceClass;
	@Version
	@Column(name = "entity_version")
	private long entityVersion;
	@Column(name = "blocked")
	@SerializedName("isBlocked")
	@JsonPolicyDef({ DEVICE_PUBLISHED, DEVICE_PUBLISHED_DEVICE_AUTH, DEVICE_SUBMITTED, NETWORK_PUBLISHED })
	private Boolean blocked;

	@SerializedName("mac")
	@Column
	// @NotNull(message = "mac field cannot be null.")
	@Size(min = 1, max = 128, message = "Field cannot be empty. The length of mac should not be more than 128 "
			+ "symbols.")
	@JsonPolicyDef({ DEVICE_PUBLISHED, DEVICE_PUBLISHED_DEVICE_AUTH, DEVICE_SUBMITTED, NETWORK_PUBLISHED })
	private String mac;

	@SerializedName("vendordeviceid")
	@Column
	// @NotNull(message = "vendordeviceid field cannot be null.")
	@Size(min = 1, max = 128, message = "Field cannot be empty. The length of vendordeviceid should not be more than 128 "
			+ "symbols.")
	@JsonPolicyDef({ DEVICE_PUBLISHED, DEVICE_PUBLISHED_DEVICE_AUTH, DEVICE_SUBMITTED, NETWORK_PUBLISHED })
	private String vendordeviceid;

	@SerializedName("weixindeviceid")
	@Column
	@Size(min = 1, max = 128, message = "Field cannot be empty. The length of weixindeviceid should not be more than 128 "
			+ "symbols.")
	@JsonPolicyDef({ DEVICE_PUBLISHED, DEVICE_PUBLISHED_DEVICE_AUTH, DEVICE_SUBMITTED, NETWORK_PUBLISHED })
	private String weixindeviceid;

	@SerializedName("product")
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "product_id")
	@JsonPolicyDef({ DEVICE_PUBLISHED, DEVICE_PUBLISHED_DEVICE_AUTH, DEVICE_SUBMITTED })
	private Product product;

	@SerializedName("firmwareversion")
	@Column(name = "firmware_version")
	@Size(min = 1, max = 50, message = "Field cannot be empty. The length of weixindeviceid should not be more than 128 "
			+ "symbols.")
	@JsonPolicyDef({ DEVICE_PUBLISHED, DEVICE_PUBLISHED_DEVICE_AUTH, DEVICE_SUBMITTED, NETWORK_PUBLISHED })
	private String firmwareVersion;
	
	@SerializedName("ip")
	@Column(name = "ip")
	@Size(min = 1, max = 128, message = "Field cannot be empty. The length of weixindeviceid should not be more than 128 "
			+ "symbols.")
	@JsonPolicyDef({ DEVICE_PUBLISHED, DEVICE_PUBLISHED_DEVICE_AUTH, DEVICE_SUBMITTED, NETWORK_PUBLISHED })
	private String ip;
	
	

	public String getFirmwareVersion() {
		return firmwareVersion;
	}

	public void setFirmwareVersion(String firmwareVersion) {
		this.firmwareVersion = firmwareVersion;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}
	
	 public String getVendordeviceid() {
	 return vendordeviceid;
	 }
	
	 public void setVendordeviceid(String vendordeviceid) {
	 this.vendordeviceid = vendordeviceid;
	 }

	public String getWeixindeviceid() {
		return weixindeviceid;
	}

	public void setWeixindeviceid(String weixindeviceid) {
		this.weixindeviceid = weixindeviceid;
	}

	public long getEntityVersion() {
		return entityVersion;
	}

	public void setEntityVersion(long entityVersion) {
		this.entityVersion = entityVersion;
	}

	/*
	 * public JsonStringWrapper getData() { return data; }
	 * 
	 * public void setData(JsonStringWrapper data) { this.data = data; }
	 */

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	/*
	 * public String getKey() { return dkey; }
	 * 
	 * public void setKey(String key) { this.dkey = key; }
	 */

	public String getName() {
		return name;
	}

	// public String getDkey() {
	// return dkey;
	// }
	//
	// public void setDkey(String dkey) {
	// this.dkey = dkey;
	// }

	@Override
	public String toString() {
		return "Device [id=" + id + ", guid=" + guid + ", mac=" + mac + ", product=" + product + ", firmwareVersion="
				+ firmwareVersion + ", ip=" + ip + "]";
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Network getNetwork() {
		return network;
	}

	public void setNetwork(Network network) {
		this.network = network;
	}

	public DeviceClass getDeviceClass() {
		return deviceClass;
	}

	public void setDeviceClass(DeviceClass deviceClass) {
		this.deviceClass = deviceClass;
	}

	public Boolean getBlocked() {
		return blocked;
	}

	public void setBlocked(Boolean blocked) {
		this.blocked = blocked;
	}

	public static class Queries {

		public static interface Parameters {

			static final String GUID = "guid";
			static final String KEY = "dkey";
			static final String ID = "id";
		}
	}
}