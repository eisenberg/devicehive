package com.devicehive.model.updates;

import java.util.Optional;

import com.devicehive.model.HiveEntity;

public class ProductUpdate implements HiveEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Optional<String> tenant;
	private Optional<String> product;
	public Optional<String> getTenant() {
		return tenant;
	}
	public void setTenant(Optional<String> tenant) {
		this.tenant = tenant;
	}
	public Optional<String> getProduct() {
		return product;
	}
	public void setProduct(Optional<String> product) {
		this.product = product;
	}

}
