package com.devicehive.model.updates;

public interface GuidGenerateInterface {

	public String generateGuid(DeviceUpdate deviceUpdate);
}
