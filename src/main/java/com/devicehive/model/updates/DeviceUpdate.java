package com.devicehive.model.updates;


import com.devicehive.json.strategies.JsonPolicyDef;
import com.devicehive.model.*;
import com.google.gson.annotations.SerializedName;

import static com.devicehive.json.strategies.JsonPolicyDef.Policy.*;

import org.springframework.beans.factory.annotation.Autowired;

public class DeviceUpdate implements HiveEntity {

    private static final long serialVersionUID = -7498444232044147881L;
    
    @Autowired
    private GuidGenerateInterface guidGenerate;
    
    
    @SerializedName("id")
    @JsonPolicyDef({DEVICE_PUBLISHED, NETWORK_PUBLISHED})
    private NullableWrapper<String> guid;

    @SerializedName("key")
    @JsonPolicyDef({DEVICE_SUBMITTED, DEVICE_PUBLISHED})
    private NullableWrapper<String> key;

    @SerializedName("name")
    @JsonPolicyDef({DEVICE_PUBLISHED, DEVICE_SUBMITTED, NETWORK_PUBLISHED})
    private NullableWrapper<String> name;

    @SerializedName("status")
    @JsonPolicyDef({DEVICE_PUBLISHED, DEVICE_SUBMITTED, NETWORK_PUBLISHED})
    private NullableWrapper<String> status;

    @SerializedName("data")
    @JsonPolicyDef({DEVICE_PUBLISHED, DEVICE_SUBMITTED, NETWORK_PUBLISHED})
    private NullableWrapper<JsonStringWrapper> data;

    @SerializedName("network")
    @JsonPolicyDef({DEVICE_PUBLISHED, DEVICE_SUBMITTED})
    private NullableWrapper<Network> network;

    @SerializedName("deviceClass")
    @JsonPolicyDef({DEVICE_PUBLISHED, DEVICE_SUBMITTED, NETWORK_PUBLISHED})
    private NullableWrapper<DeviceClassUpdate> deviceClass;

    @JsonPolicyDef({DEVICE_SUBMITTED, DEVICE_PUBLISHED})
    @SerializedName("blocked")
    private NullableWrapper<Boolean> blocked;
    
    @JsonPolicyDef({DEVICE_SUBMITTED, DEVICE_PUBLISHED})
    @SerializedName("mac")
    private NullableWrapper<String> mac;
    
    @JsonPolicyDef({DEVICE_SUBMITTED, DEVICE_PUBLISHED})
    @SerializedName("ip")
    private NullableWrapper<String> ip;
    
    @JsonPolicyDef({DEVICE_SUBMITTED, DEVICE_PUBLISHED})
    @SerializedName("tenant")
    private NullableWrapper<String> tenant;
    
    @JsonPolicyDef({DEVICE_SUBMITTED, DEVICE_PUBLISHED})
    @SerializedName("model")
    private NullableWrapper<String> model;
    
    @JsonPolicyDef({DEVICE_SUBMITTED, DEVICE_PUBLISHED})
    @SerializedName("firmwareversion")
    private NullableWrapper<String> firmwareversion;

    public NullableWrapper<DeviceClassUpdate> getDeviceClass() {
        return deviceClass;
    }

    public void setDeviceClass(NullableWrapper<DeviceClassUpdate> deviceClass) {
        this.deviceClass = deviceClass;
    }

    public NullableWrapper<String> getGuid() {
        return guid;
    }

    public void setGuid(NullableWrapper<String> guid) {
        this.guid = guid;
    }

    public NullableWrapper<String> getKey() {
        return key;
    }

    public void setKey(NullableWrapper<String> key) {
        this.key = key;
    }

    public NullableWrapper<String> getName() {
        return name;
    }

    public void setName(NullableWrapper<String> name) {
        this.name = name;
    }

    public NullableWrapper<String> getStatus() {
        return status;
    }

    public void setStatus(NullableWrapper<String> status) {
        this.status = status;
    }

    public NullableWrapper<JsonStringWrapper> getData() {
        return data;
    }

    public void setData(NullableWrapper<JsonStringWrapper> data) {
        this.data = data;
    }

    public NullableWrapper<Network> getNetwork() {
        return network;
    }

    public void setNetwork(NullableWrapper<Network> network) {
        this.network = network;
    }

    public NullableWrapper<Boolean> getBlocked() {
        return blocked;
    }

    public void setBlocked(NullableWrapper<Boolean> blocked) {
        this.blocked = blocked;
    }

    public NullableWrapper<String> getMac() {
		return mac;
	}

	public void setMac(NullableWrapper<String> mac) {
		this.mac = mac;
	}

	public NullableWrapper<String> getIp() {
		return ip;
	}

	public void setIp(NullableWrapper<String> ip) {
		this.ip = ip;
	}

	public NullableWrapper<String> getTenant() {
		return tenant;
	}

	public void setTenant(NullableWrapper<String> tenant) {
		this.tenant = tenant;
	}

	public NullableWrapper<String> getModel() {
		return model;
	}

	public void setModel(NullableWrapper<String> model) {
		this.model = model;
	}

	public NullableWrapper<String> getFirmwareversion() {
		return firmwareversion;
	}

	public void setFirmwareversion(NullableWrapper<String> firmwareversion) {
		this.firmwareversion = firmwareversion;
	}

	public Device convertTo() {
		guidGenerate = new DefaultGuidGenerateImpl();//urgly
        Device device = new Device();
        if (guid != null) {
            device.setGuid(guidGenerate.generateGuid(this));
            device.setVendordeviceid(guid.getValue());
        }
        if (data != null) {
            device.setData(data.getValue().getJsonString());
        }
        if (deviceClass != null) {
            DeviceClass convertedDeviceClass = deviceClass.getValue().convertTo();
            device.setDeviceClass(convertedDeviceClass);
        }
        if (name != null) {
            device.setName(name.getValue());
        }
        if (network != null) {
            device.setNetwork(network.getValue());
        }
        if (status != null) {
            device.setStatus(status.getValue());
        }
        if (blocked != null) {
            device.setBlocked(Boolean.TRUE.equals(blocked.getValue()));
        }
        if(firmwareversion != null){
        	device.setFirmwareVersion(firmwareversion.getValue());
        }
        if(ip != null){
        	device.setIp(ip.getValue());
        }
        if(mac != null){
        	device.setMac(mac.getValue());
        }
        return device;
    }
}
