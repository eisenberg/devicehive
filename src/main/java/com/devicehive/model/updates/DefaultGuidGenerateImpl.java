package com.devicehive.model.updates;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class DefaultGuidGenerateImpl implements GuidGenerateInterface {

	@Override
	public String generateGuid(DeviceUpdate deviceUpdate) {
		
		return deviceUpdate.getGuid().getValue();
	}

}
