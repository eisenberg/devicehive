package com.devicehive.model;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.devicehive.json.strategies.JsonPolicyDef;
import com.devicehive.json.strategies.JsonPolicyDef.Policy;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "product")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({
		@NamedQuery(name="Product.queryByName",query="select p from Product p where p.name=:name"),
		@NamedQuery(name="Product.getUserByName",query="select p.user from Product p where p.name=:name"),
		@NamedQuery(name="Product.exitByName",query="select count(distinct p) from Product p where p.name=:name"),
		@NamedQuery(name="Product.findByLicence",query="select count(distinct p) from Product p where p.licence=:licence"),
		@NamedQuery(name="Product.findProductByLicence",query="select p from Product p where p.licence=:licence"),
		@NamedQuery(name="Product.findByNameAndUser",query="select p from Product p "
		+"left join fetch p.user u where p.name=:name and u.login=:login"),
		@NamedQuery(name="Product.findByLicenceNameAndUser",query="select p from Product p "
				+"left join fetch p.user u where p.licence=:licence and p.name=:name")
})
public class Product implements HiveEntity {
	private static final long serialVersionUID = -280552941054814125L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column
	@SerializedName("product")
	@NotNull(message = "Key column cannot be null")
	@Size(min = 1, max = 48, message = "Field cannot be empty. The length of guid should not be more than 48 symbols.")
	@JsonPolicyDef({Policy.PRODUCT_PUBLISH})
	private String name;
	@Column
	@NotNull(message = "Key column cannot be null")
	@Size(min = 1, max = 100, message = "Field cannot be empty. The length of guid should not be more than 48 symbols.")
	@JsonPolicyDef({Policy.PRODUCT_PUBLISH})
	private String licence;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", updatable = false)
	@NotNull(message = "User column cannot be null")
	private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLicence() {
		return licence;
	}

	public void setLicence(String licence) {
		this.licence = licence;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", licence=" + licence + ", user=" + user + "]";
	}
}
