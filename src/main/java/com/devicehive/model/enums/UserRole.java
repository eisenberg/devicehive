package com.devicehive.model.enums;

/**
 * Enum for User roles in the system.
 */
public enum UserRole {
    ADMIN(0),
    CLIENT(1), 
	TENANT(2),
	OAUTH(3);
	

    private final int value;

    UserRole(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.name();
    }
}
