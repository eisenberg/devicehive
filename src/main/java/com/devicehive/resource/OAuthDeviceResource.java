package com.devicehive.resource;

import com.devicehive.json.strategies.JsonPolicyApply;
import com.devicehive.json.strategies.JsonPolicyDef;
import com.devicehive.model.updates.UserUpdate;
import com.devicehive.model.wrappers.DeviceCommandWrapper;
import com.wordnik.swagger.annotations.*;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api(tags = { "oauth" })
@Path("/oauth")
public interface OAuthDeviceResource {

	/**
	 * This method will generate following output
	 * <p/>
	 * <code> [ { "id": 2, "login": "login", "role": 0, "status": 0,
	 * "lastLogin": "1970-01-01 03:00:00.0" }, { "id": 3, "login": "login1", "role": 1, "status": 2, "lastLogin":
	 * "1970-01-01 03:00:00.0" } ] </code>
	 *
	 * @param login
	 *            user login ignored, when loginPattern is specified
	 * @param loginPattern
	 *            login pattern (LIKE %VALUE%) user login will be ignored, if
	 *            not null
	 * @param role
	 *            User's role ADMIN - 0, CLIENT - 1
	 * @param status
	 *            ACTIVE - 0 (normal state, user can logon) , LOCKED_OUT - 1
	 *            (locked for multiple login failures), DISABLED - 2 , DELETED -
	 *            3;
	 * @param sortField
	 *            either of "login", "loginAttempts", "role", "status",
	 *            "lastLogin"
	 * @param sortOrderSt
	 *            either ASC or DESC
	 * @param take
	 *            like SQL LIMIT
	 * @param skip
	 *            like SQL OFFSET
	 * @return List of User
	 */
	@POST
    @Consumes(MediaType.APPLICATION_JSON)
	@Path("/binddevice")
	@PreAuthorize("hasAnyRole('ADMIN', 'KEY') and hasPermission(null, 'MANAGE_USER')")
	@ApiOperation(value = "binddevice")
	Response bindDevice(
			@ApiParam(name = "vendordeviceid", value = "vendordeviceid") @QueryParam("vendordeviceid") String vendordeviceid,
			@ApiParam(name = "weixindeviceid", value = "weixindeviceid") @QueryParam("weixindeviceid") String weixindeviceid,
			@ApiParam(value = "User body", defaultValue = "{}")UserUpdate userToCreate);
	@GET
	@Path("/unbinddevice")
	@PreAuthorize("hasAnyRole('ADMIN','KEY') and hasPermission(null, 'MANAGE_USER')")
	@ApiOperation(value = "unbindDevice")
	Response unbindDevice(
			@ApiParam(name = "vendordeviceid", value = "vendordeviceid") @QueryParam("vendordeviceid") String vendordeviceid,
			@ApiParam(name = "openid", value = "openid") @QueryParam("openid") String openid
	);
	
	@GET
	@Path("/cntstatus")
	@PreAuthorize("hasAnyRole('ADMIN', 'KEY') and hasPermission(null, 'MANAGE_USER')")
	@ApiOperation(value = "cntstatus")
	Response getStatus(@ApiParam(name = "vendordeviceid", value = "vendordeviceid") @QueryParam("vendordeviceid") String vendordeviceid,
			@ApiParam(name = "openid", value = "openid") @QueryParam("openid") String openid
	);
}
