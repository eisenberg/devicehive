package com.devicehive.resource;

import com.devicehive.json.strategies.JsonPolicyDef;
import com.devicehive.model.updates.ProductUpdate;
import com.devicehive.model.updates.UserUpdate;
import com.wordnik.swagger.annotations.*;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api(tags = { "tenant" })
@Path("/tenant/product")
public interface TenantProductResource {
	/**
	 * One needs to provide user resource in request body (all parameters are
	 * mandatory):
	 * <p/>
	 * <code> { "login":"login"
	 * "role":0 "status":0 "password":"qwerty" } </code>
	 * <p/>
	 * In case of success server will provide following response with code 201
	 * <p/>
	 * <code> { "id": 1, "lastLogin": null } </code>
	 *
	 * @return Empty body, status 201 if success, 403 if forbidden, 400
	 *         otherwise
	 */
	@POST
	@Path("/register")
	@Consumes(MediaType.APPLICATION_JSON)
	@PreAuthorize("hasAnyRole('SESSION','ADMIN', 'KEY') and hasPermission(null, 'MANAGE_USER')")
	@JsonPolicyDef(JsonPolicyDef.Policy.USERS_LISTED)
	@ApiOperation(value = "Create tenant product")
	Response register(@ApiParam(value = "product body", defaultValue = "{}", required = true) ProductUpdate productToCreate);
}
