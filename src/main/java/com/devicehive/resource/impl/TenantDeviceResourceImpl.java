package com.devicehive.resource.impl;

import static com.devicehive.configuration.Constants.DEVICE_CLASS;
import static com.devicehive.configuration.Constants.NAME;
import static com.devicehive.configuration.Constants.NETWORK;
import static com.devicehive.configuration.Constants.STATUS;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.OK;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import com.devicehive.auth.HivePrincipal;
import com.devicehive.configuration.Messages;
import com.devicehive.json.strategies.JsonPolicyDef;
import com.devicehive.json.tenant.model.DeviceCriteriaPropertyConstants;
import com.devicehive.model.Device;
import com.devicehive.model.ErrorResponse;
import com.devicehive.model.User;
import com.devicehive.model.response.BatchInsertResponse;
import com.devicehive.model.response.DeviceSearchResponse;
import com.devicehive.model.response.DeviceSearchResponse.ReturnSearchDevice;
import com.devicehive.model.tenant.TenantDeviceList;
import com.devicehive.resource.converters.SortOrderQueryParamParser;
import com.devicehive.resource.util.ResponseFactory;
import com.devicehive.service.DeviceService;
import com.devicehive.service.TenantDeviceService;
import com.devicehive.service.TenantUserService;
import com.google.common.collect.ImmutableSet;
import com.google.gson.JsonObject;

public class TenantDeviceResourceImpl implements com.devicehive.resource.TenantDeviceResource {
	private static final Logger logger = LoggerFactory.getLogger(UserResourceImpl.class);
	@Autowired
	private TenantDeviceService tenantDeviceService;

	@Override
	public Response register(TenantDeviceList deviceList) {
		List<Device> deviceReturn = tenantDeviceService.batchInsertDevices(deviceList);
		
		List<String> ids = new ArrayList<String>(deviceReturn.size());
		deviceReturn.stream().forEach(device->{
			ids.add(device.getGuid());
		});
//
//		HivePrincipal hivePrincipal = (HivePrincipal) SecurityContextHolder.getContext().getAuthentication()
//				.getPrincipal();
//		Gson mainGson = GsonFactory.createGson(DEVICE_SUBMITTED);
//		TenantDeviceList device = mainGson.fromJson(jsonObject, TenantDeviceList.class);
//		if (StringUtils.isBlank(device.getMac()) || StringUtils.isBlank(device.getDeviceID()))
//			throw new CommonException("device mac or deviceid is null", 400);
	

		return ResponseFactory.response(OK, new BatchInsertResponse(200,ImmutableSet.copyOf(ids)),
				JsonPolicyDef.Policy.DEVICE_NOT_INSERT);
	}

	@Override
	public Response search(Integer numonly, Integer take, Integer skip, JsonObject deviceCriteria) {
		logger.info("@@@@@@numonly:{},take:{},skip:{}",numonly,take,skip);
		List<Device> devices = tenantDeviceService.getList(deviceCriteria, take, skip);
		ReturnSearchDevice info = new ReturnSearchDevice();
		JsonPolicyDef.Policy policy = null;
		if(numonly == 0){
			info.setTotal(devices.size());
			info.setDevices(ImmutableSet.copyOf(devices));
			policy = JsonPolicyDef.Policy.DEVICE_SEARCH_RETURN;
		}else{
			info.setTotal(devices.size());
			policy = JsonPolicyDef.Policy.DEVICE_SEARCH_NUM_ONLY;
		}
		return ResponseFactory.response(OK, new DeviceSearchResponse(200,info),policy);
	}
}
