package com.devicehive.resource.impl;

import static javax.ws.rs.core.Response.Status.OK;

import java.util.concurrent.ExecutorService;

import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import com.devicehive.application.DeviceHiveApplication;
import com.devicehive.exceptions.CommonException;
import com.devicehive.messages.kafka.CommandConsumer;
import com.devicehive.model.AccessKey;
import com.devicehive.model.Device;
import com.devicehive.model.User;
import com.devicehive.model.response.StatusResponse;
import com.devicehive.model.updates.UserUpdate;
import com.devicehive.model.wrappers.DeviceCommandWrapper;
import com.devicehive.resource.OAuthDeviceResource;
import com.devicehive.resource.util.ResponseFactory;
import com.devicehive.service.DeviceActivityService;
import com.devicehive.service.OauthService;
import com.devicehive.websockets.util.SessionMonitor;

@Service
public class OAuthDeviceResourceImpl implements OAuthDeviceResource {
	private static final Logger logger = LoggerFactory.getLogger(CommandConsumer.class);
	@Autowired
	private DeviceActivityService activityService;
	@Autowired
	@Qualifier(DeviceHiveApplication.MESSAGE_EXECUTOR)
	private ExecutorService mes;
	@Autowired
	private OauthService oauthService;

	@Autowired
	private SessionMonitor sessionMonitor;

	@Override
	public Response bindDevice(String vendordeviceid, String weixindeviceid, UserUpdate userUpdate) {
		Device device = oauthService.findDevice(vendordeviceid, weixindeviceid, userUpdate);
		User user = oauthService.findUser(userUpdate);
		oauthService.bindByUser(device, user, -1);
	    String weixindid = device.getWeixindeviceid();
		return ResponseFactory.response(OK, new StatusResponse(200, weixindid));
	}

	@Override
	public Response unbindDevice(String vendordeviceid, String openid) {
		oauthService.unBindDevice(vendordeviceid, openid);
		return ResponseFactory.response(OK, new StatusResponse(200, "success"));
	}

	@Override
	public Response getStatus(String vendordeviceid, String openid) {
		if (null == vendordeviceid || null == openid || "".equals(vendordeviceid.trim()) || "".equals(openid.trim())) {
			throw new CommonException("param invalidate", 400);
		}
		oauthService.accessDevice(vendordeviceid, openid);
		String sessionId = sessionMonitor.getGuid(vendordeviceid);
        if(null == sessionId){
        	return ResponseFactory.response(OK, new StatusResponse(200, "offline"));
        }
		WebSocketSession webSocketSession = sessionMonitor.getSession(sessionId);
		if (null != webSocketSession) {
			if (webSocketSession.isOpen()) {
				return ResponseFactory.response(OK, new StatusResponse(200, "online"));
			} else {
				return ResponseFactory.response(OK, new StatusResponse(200, "offline"));
			}
		} 
		
		boolean online = activityService.online(vendordeviceid);
		if (online) {
			return ResponseFactory.response(OK, new StatusResponse(200, "online"));
		} else {
			return ResponseFactory.response(OK, new StatusResponse(200, "offline"));
		}
	}
}
