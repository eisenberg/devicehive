package com.devicehive.resource.impl;

import static javax.ws.rs.core.Response.Status.OK;

import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import com.devicehive.auth.HivePrincipal;
import com.devicehive.exceptions.CommonException;
import com.devicehive.json.strategies.JsonPolicyDef.Policy;
import com.devicehive.model.Product;
import com.devicehive.model.User;
import com.devicehive.model.response.ProductInsetResponse;
import com.devicehive.model.response.StatusResponse;
import com.devicehive.model.updates.ProductUpdate;
import com.devicehive.model.updates.UserUpdate;
import com.devicehive.resource.TenantProductResource;
import com.devicehive.resource.util.ResponseFactory;
import com.devicehive.service.CacheService;
import com.devicehive.service.CacheService.SessionToken;
import com.devicehive.service.TenantProductService;
import com.devicehive.service.TenantUserService;
import com.devicehive.service.UserService;


public class TenantProductResourceImpl implements TenantProductResource {
	private static final Logger logger = LoggerFactory.getLogger(TenantProductResourceImpl.class);

	@Autowired
	private TenantProductService productService;

	@Override
	public Response register(ProductUpdate productToCreate) {
		Product product = productService.ceateProduct(productToCreate);
		 
        return ResponseFactory.response(OK, new ProductInsetResponse(200,product),Policy.PRODUCT_PUBLISH);
	}
}
