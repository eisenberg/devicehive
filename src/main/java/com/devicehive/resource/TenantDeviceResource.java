package com.devicehive.resource;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.security.access.prepost.PreAuthorize;

import com.devicehive.json.tenant.model.DeviceCriteriaPropertyConstants;
import com.devicehive.model.tenant.TenantDeviceList;
import com.google.gson.JsonObject;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

/**
 * REST controller for devices: <i>/device</i>. See <a href="http://www.devicehive.com/restful#Reference/Device">DeviceHive
 * RESTful API: Device</a> for details.
 */
@Api(tags = { "tenant" })
@Path("/tenant/device")
public interface TenantDeviceResource {
    /**
     * Implementation of <a href="http://www.devicehive.com/restful#Reference/Device/register">DeviceHive RESTful API:
     * Device: register</a> Registers a device. If device with specified identifier has already been registered, it gets
     * updated in case when valid key is provided in the authorization header.
     *
     * @param jsonObject In the request body, supply a Device resource. See <a href="http://www.devicehive
     *                   .com/restful#Reference/Device/register">
     * @param deviceGuid Device unique identifier.
     * @return response code 201, if successful
     */
    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    @PreAuthorize("hasAnyRole('CLIENT', 'ADMIN', 'KEY','SESSION') and hasPermission(null, 'GET_DEVICE')")
    @ApiOperation(value = "Register device batch", notes = "Registers device and do additional steps - create device class, network, equipment")
    Response register(
            @ApiParam(value = "DeviceList body", required = true, defaultValue = "{}")
            TenantDeviceList deviceList);
    /**
     * Implementation of <a href="http://www.devicehive.com/restful#Reference/Device/get">DeviceHive RESTful API:
     * Device: get</a> Gets information about device.
     *
     * @param guid Device unique identifier
     * @return If successful, this method returns a <a href="http://www.devicehive.com/restful#Reference/Device">Device</a>
     *         resource in the response body.
     */
    @POST
    @Path("/search")
    @PreAuthorize("hasAnyRole('CLIENT', 'ADMIN', 'KEY') and hasPermission(null, 'GET_DEVICE')")
    @ApiOperation(value = "search device", notes = "Returns device by guid")
    @ApiResponses({
            @ApiResponse(code = 404, message = "If device not found")
    })
    Response search(
            @ApiParam(name = "numonly", value = "numonly", defaultValue = "0")
            @QueryParam("numonly")
            @Min(0) @Max(Integer.MAX_VALUE)
            @DefaultValue("0")
            Integer numonly,
            @ApiParam(name = "take", value = "Limit param", defaultValue = "100")
            @QueryParam("take")
            @Min(1) @Max(Integer.MAX_VALUE)
            @DefaultValue("100")
            Integer take,
            @ApiParam(name = "skip", value = "Skip param", defaultValue = "0")
            @QueryParam("skip")
            @Min(0) @Max(Integer.MAX_VALUE)
            @DefaultValue("0")
            Integer skip,
            @ApiParam(value = "Device body", required = true, defaultValue = "{}")
            JsonObject deviceCriteria
    		);
}
