package com.devicehive.auth.rest.providers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

public class DeviceAuthenticationProvider implements AuthenticationProvider {
    private static final Logger logger = LoggerFactory.getLogger(DeviceAuthenticationProvider.class);

/*    @Autowired
    private DeviceService deviceService;

    @Autowired
    private DeviceActivityService deviceActivityService;

    @Autowired
    private TenantProductService tenantProductService;
    */
    @Autowired
    private DeviceAuthenticater deviceAuthenticate;
    
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String deviceId = (String) authentication.getPrincipal();
        String deviceKey = (String) authentication.getCredentials();
     /*   logger.debug("Device authentication requested for device {}", deviceId);
        boolean licence = false;
		if (StringUtils.isNoneBlank(StringUtils.trim(deviceKey))) {
			if (!tenantProductService.authenticateLicence(StringUtils.trim(deviceKey))) {
				throw new BadCredentialsException("Invalid credentials");
			}
			licence = true;
		}
        Device device = deviceService.authenticate(deviceId,licence);
        if (device != null && !Boolean.TRUE.equals(device.getBlocked())) {
            logger.info("Device authentication for {} succeeded", device.getGuid());
            deviceActivityService.update(device.getGuid());
            return new HiveAuthentication(
                    new HivePrincipal(device),
                    AuthorityUtils.createAuthorityList(HiveRoles.DEVICE));
        }
        logger.error("Can't find device in the storage deviceId {} ", deviceId);
        throw new BadCredentialsException("Invalid credentials");*/
        return deviceAuthenticate.authenticate(deviceId, deviceKey);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return DeviceAuthenticationToken.class.equals(authentication);
    }

}
