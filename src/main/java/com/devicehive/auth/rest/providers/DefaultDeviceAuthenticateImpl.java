package com.devicehive.auth.rest.providers;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

import com.devicehive.auth.HiveAuthentication;
import com.devicehive.auth.HivePrincipal;
import com.devicehive.auth.HiveRoles;
import com.devicehive.model.Device;
import com.devicehive.service.DeviceActivityService;
import com.devicehive.service.DeviceService;
import com.devicehive.service.TenantProductService;
@Component
public class DefaultDeviceAuthenticateImpl implements DeviceAuthenticater {
	private static final Logger logger = LoggerFactory.getLogger(DeviceAuthenticationProvider.class);
	@Autowired
	private DeviceService deviceService;

	@Autowired
	private DeviceActivityService deviceActivityService;

	@Autowired
	private TenantProductService tenantProductService;

	@Override
	public Authentication authenticate(String deviceId, String deviceKey) {
		boolean licence = false;
		if (StringUtils.isNoneBlank(StringUtils.trim(deviceKey))) {
			if (!tenantProductService.authenticateLicence(deviceKey)) {
				throw new BadCredentialsException("Invalid credentials");
			}
			licence = true;
		}
		Device device = deviceService.authenticate(deviceId, licence);
		if (device != null && !Boolean.TRUE.equals(device.getBlocked())) {
			logger.info("Device authentication for {} succeeded", device.getGuid());
			deviceActivityService.update(device.getGuid());
			return new HiveAuthentication(new HivePrincipal(device),
					AuthorityUtils.createAuthorityList(HiveRoles.DEVICE));
		}
		logger.error("Can't find device in the storage deviceId {} ", deviceId);
		throw new BadCredentialsException("Invalid credentials");
	}

}
