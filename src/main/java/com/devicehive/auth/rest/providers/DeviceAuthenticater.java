package com.devicehive.auth.rest.providers;

import org.springframework.security.core.Authentication;

public interface DeviceAuthenticater {

	public Authentication authenticate(String deviceId, String deviceKey);

}
