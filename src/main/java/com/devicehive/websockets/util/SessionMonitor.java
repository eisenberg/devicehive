package com.devicehive.websockets.util;

import java.io.IOException;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.Lock;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.PingMessage;
import org.springframework.web.socket.WebSocketSession;

import com.devicehive.auth.HivePrincipal;
import com.devicehive.configuration.Constants;
import com.devicehive.messages.bus.MessageBus;
import com.devicehive.messages.subscriptions.CommandSubscription;
import com.devicehive.messages.subscriptions.SubscriptionManager;
import com.devicehive.model.Device;
import com.devicehive.model.DeviceNotification;
import com.devicehive.model.SpecialNotifications;
import com.devicehive.service.DeviceActivityService;
import com.devicehive.service.time.TimestampService;
import com.devicehive.websockets.HiveWebsocketSessionState;

@Component
public class SessionMonitor {
	private static final Logger logger = LoggerFactory.getLogger(SessionMonitor.class);

	private ConcurrentMap<String, WebSocketSession> sessionMap;
	private ConcurrentMap<String, String> deviceSessionMap;

	@Autowired
	private DeviceActivityService deviceActivityService;
	@Autowired
	private SubscriptionManager subscriptionManager;
	@Autowired
	private MessageBus messageBus;

	@Autowired
	private TimestampService timestampService;

	public void registerSession(final WebSocketSession session) {
		sessionMap.put(session.getId(), session);
	}

	public void registerDeviceSession(final WebSocketSession session, String guid) {
		deviceSessionMap.put(guid, session.getId());
		messageBus.publish(getNotification(guid,"connect"), false);
	}

	public String getGuid(String guid) {
		return deviceSessionMap.get(guid);
	}

	public WebSocketSession getSession(String sessionId) {
		WebSocketSession session = sessionMap.get(sessionId);
		return session != null && session.isOpen() ? session : null;
	}

	public void updateDeviceSession(WebSocketSession session) {
		HivePrincipal hivePrincipal = HiveWebsocketSessionState.get(session).getHivePrincipal();
		Device authorizedDevice = hivePrincipal != null ? hivePrincipal.getDevice() : null;
		if (authorizedDevice != null) {
			deviceActivityService.update(authorizedDevice.getGuid());
		}
		Set<UUID> commandSubscriptions = HiveWebsocketSessionState.get(session).getCommandSubscriptions();
		for (UUID subId : commandSubscriptions) {
			for (CommandSubscription subscription : subscriptionManager.getCommandSubscriptionStorage().get(subId)) {
				if (subscription.getDeviceGuid() != Constants.NULL_SUBSTITUTE) {
					deviceActivityService.update(subscription.getDeviceGuid());
				}
			}
		}
	}

	@Scheduled(cron = "0/30 * * * * *")
	public synchronized void ping() {
		for (WebSocketSession session : sessionMap.values()) {
			if (session.isOpen()) {
				logger.debug("Pinging session " + session.getId());
				Lock lock = HiveWebsocketSessionState.get(session).getQueueLock();
				lock.lock();
				try {
					session.sendMessage(new PingMessage(Constants.PING));
				} catch (IOException ex) {
					logger.error("Error sending ping", ex);
					closePing(session);
				} finally {
					lock.unlock();
				}
			} else {
				logger.debug("Session " + session.getId() + " is closed.");
				sessionMap.remove(session.getId());
				//DeviceNotification notification = new DeviceNotification();
				// dn.setDeviceGuid();
				String guid = deviceSessionMap.get(session.getId());
				// notification =
				// ServerResponsesFactory.createNotificationForDevice(device,
				// SpecialNotifications.DEVICE_UPDATE);
				//notification.setNotification(SpecialNotifications.DEVICE_UPDATE);
				//notification.setTimestamp(timestampService.getTimestamp());
				//notification.setId(Math.abs(new Random().nextInt()));
				//notification.setDeviceGuid(guid);
				DeviceNotification notification = getNotification(guid,"disconnect");
				
				messageBus.publish(notification, false);
			}
		}
	}

	public DeviceNotification getNotification(String guid, String notif) {
		DeviceNotification notification = new DeviceNotification();
		notification.setNotification(SpecialNotifications.DEVICE_UPDATE);
		notification.setTimestamp(timestampService.getTimestamp());
		notification.setId(Math.abs(new Random().nextInt()));
        StringBuilder sb = new StringBuilder();
        sb.append(guid).append(":").append(notif);
		notification.setDeviceGuid(sb.toString());
		return notification;

	}

	public void closePing(WebSocketSession session) {
		try {
			session.close(CloseStatus.NO_CLOSE_FRAME);
		} catch (IOException ex) {
			logger.error("Error closing session", ex);
		} finally {
			deviceSessionMap.remove(session.getId());
		}
	}

	@PostConstruct
	public void init() {
		sessionMap = new ConcurrentHashMap<>();
		deviceSessionMap = new ConcurrentHashMap<String, String>();
	}

	@PreDestroy
	public void closeAllSessions() {
		for (WebSocketSession session : sessionMap.values()) {
			try {
				session.close(CloseStatus.SERVICE_RESTARTED);
			} catch (IOException ex) {
				logger.error("Error closing session", ex);
			}
		}
		sessionMap.clear();
	}
}
