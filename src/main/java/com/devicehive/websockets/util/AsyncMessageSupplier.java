package com.devicehive.websockets.util;

import com.devicehive.application.DeviceHiveApplication;
import com.devicehive.configuration.Constants;
import com.devicehive.json.GsonFactory;
import com.devicehive.json.adapters.TimestampAdapter;
import com.devicehive.model.DeviceCommand;
import com.devicehive.service.HazelcastEntityCacheService;
import com.devicehive.websockets.HiveWebsocketSessionState;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.hazelcast.core.IMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedQueue;


@Component
public class AsyncMessageSupplier {
    private static final Logger logger = LoggerFactory.getLogger(AsyncMessageSupplier.class);
    @Autowired
    private HazelcastEntityCacheService cacheService;
    
    @Async(DeviceHiveApplication.MESSAGE_EXECUTOR)
    public void deliverMessages(WebSocketSession session) {
        ConcurrentLinkedQueue<JsonElement> queue = HiveWebsocketSessionState.get(session).getQueue();
        boolean acquired = false;
        try {
            acquired = HiveWebsocketSessionState.get(session).getQueueLock().tryLock();
            if (acquired) {
                while (!queue.isEmpty()) {
                    JsonElement jsonElement = queue.peek();
                    if (jsonElement == null) {
                        queue.poll();
                        continue;
                    }
                    if (session.isOpen()) {
                        String data = GsonFactory.createGson().toJson(jsonElement);
                        session.sendMessage(new TextMessage(data));
                        removeHazelcastEntityCache(jsonElement);
                        queue.poll();
                    } else {
                        logger.error("Session is closed. Unable to deliver message");
                        queue.clear();
                        return;
                    }
                    logger.debug("Session {}: {} messages left", session.getId(), queue.size());
                }
            }
        } catch (IOException e) {
            logger.error("Unexpected exception", e);
            throw new RuntimeException(e);
        } finally {
            if (acquired) {
                HiveWebsocketSessionState.get(session).getQueueLock().unlock();
            }
        }
    }
    
    private void removeHazelcastEntityCache(JsonElement jsonElement){
    	JsonObject jsonObject = (JsonObject)jsonElement;
    	if(null != jsonObject.get(Constants.COMMAND)){
    		JsonObject deviceCommandJson = (JsonObject)jsonObject.get(Constants.COMMAND);
    		String key = null;
			if (null != deviceCommandJson) {
				try{
					key = getHazelcastKey(deviceCommandJson);
					DeviceCommand deviceCommand = (DeviceCommand)cacheService.get(DeviceCommand.class).remove(key);
//					logger.info("@@@@ remove key {} offline devicecommand {} total map {}", key,deviceCommand,cacheService.get(DeviceCommand.class).toString());
				}catch(Exception e){
//					logger.info("@@@@ remove offline devicecommand {} fail", key);
				}
				
				
			}
    	}
    	
    }
    private String getHazelcastKey(JsonObject deviceCommandJson) {
    	Long id = deviceCommandJson.get(Constants.ID).getAsLong();
		String guid = deviceCommandJson.get(Constants.DEVICE_GUID).getAsString();
		String timestamp = deviceCommandJson.get(Constants.TIMESTAMP).getAsString();
        return id+"-"+guid+"-"+timestamp;
    }

}



