package com.devicehive.messages.kafka;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.concurrent.ExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.devicehive.application.DeviceHiveApplication;
import com.devicehive.messages.subscriptions.SubscriptionManager;
import com.devicehive.model.DeviceNotification;
import com.devicehive.service.DeviceService;

/**
 * Created by tmatvienko on 12/24/14.
 */
public class OfflineComsumer extends AbstractConsumer<DeviceNotification> {
	private static final Logger logger = LoggerFactory.getLogger(OfflineComsumer.class);

	@Autowired
	private SubscriptionManager subscriptionManager;

	@Autowired
	private DeviceService deviceService;

	@Autowired
	@Qualifier(DeviceHiveApplication.MESSAGE_EXECUTOR)
	private ExecutorService mes;

	private String url = "http://115.28.142.131/reportstatus?";

	@Override
	public void submitMessage(final DeviceNotification message) {
		String guid = message.getDeviceGuid();
		mes.submit(new Runnable() {
			@Override
			public void run() {
				URL obj;
				try {
					String[] str = guid.split(":");
					if(null == str || str.length == 2)
						return;
					String gid = str[0];
					String notification = str[1];
					StringBuilder sb = new StringBuilder();
					sb.append(url).append("vendordeviceid").append("=").append(gid).append("&").append("status")
							.append("=").append(notification);
					url = sb.toString();
					obj = new URL(url);

					HttpURLConnection con = (HttpURLConnection) obj.openConnection();

					// optional default is GET
					con.setRequestMethod("GET");

					// add request header
					// con.setRequestProperty("User-Agent", USER_AGENT);

					int responseCode = con.getResponseCode();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
