package com.devicehive.service.strategy;

import com.devicehive.model.User;

public interface LicenceGenerateStrategy {
	public abstract String generateLicence(User user,String productName);
}
