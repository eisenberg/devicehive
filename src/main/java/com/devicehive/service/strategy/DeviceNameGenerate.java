package com.devicehive.service.strategy;

import com.devicehive.model.tenant.TenantDevice;
import com.devicehive.model.tenant.TenantDeviceList;

public interface DeviceNameGenerate {
	
	public String generateName(TenantDeviceList list,TenantDevice device);

	
	
}
