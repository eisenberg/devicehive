package com.devicehive.service.strategy;

import java.util.UUID;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Component;

import com.devicehive.model.User;

@Component
public class DefaultLicenceGenerateImpl implements LicenceGenerateStrategy {
	private PasswordEncoder mPasswordEncoder;
	
	@PostConstruct
	public void init(){
		mPasswordEncoder = new StandardPasswordEncoder();
	}
	
	@Override
	public String generateLicence(User user, String productName) {
		StringBuilder sb = new StringBuilder();
		sb.append(UUID.randomUUID().toString()).append("|").append(productName);
		String licence = mPasswordEncoder.encode(sb.toString());
		return licence;
	}

}
