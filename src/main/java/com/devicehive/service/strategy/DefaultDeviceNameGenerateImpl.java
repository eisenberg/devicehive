package com.devicehive.service.strategy;

import org.springframework.stereotype.Component;

import com.devicehive.model.tenant.TenantDevice;
import com.devicehive.model.tenant.TenantDeviceList;
@Component
public class DefaultDeviceNameGenerateImpl implements DeviceNameGenerate {

	@Override
	public String generateName(TenantDeviceList list, TenantDevice device) {
		StringBuilder sb = new StringBuilder();
		sb.append(list.getTenant()).append("-").append(list.getProduct()).append(device.getDeviceID());
		return sb.toString();
	}

}
