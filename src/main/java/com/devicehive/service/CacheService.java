package com.devicehive.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

@Component
public class CacheService {
	@Autowired
	private HazelcastInstance hzInstance;
    public static  String  SESSION_TOKEN_MAP = "session_map";
    private IMap<String, Object> sessionTokenMap ;
    private ConcurrentHashMap<String,IMap<String,Object>> cache;
    
	@PostConstruct
	public void postConstruct() {
		sessionTokenMap = hzInstance.getMap(SESSION_TOKEN_MAP);
		cache = new ConcurrentHashMap<>();
		cache.put(SESSION_TOKEN_MAP, sessionTokenMap);
	}

	public <T> void put(String type ,String key, T value) {
		if(null != cache.get(type)){
			cache.get(type).put(key, value);
		}
	}
	
	public IMap<String, Object> getSessionTokenMap(){
		return sessionTokenMap;
	}
	
	public <T> T remove(String type ,String key){
		if(null != cache.get(type)){
			return (T) cache.get(type).remove(key);
		}
		return null;
	}

	public <T> T get(String type ,String key) {
		if(null != cache.get(type)){
			return (T) cache.get(type).get(key);
		}
		return null;
	}
	@Scheduled(cron = "0 */15 * * * *")
	public void cleanSession(){
		List<String> list = new ArrayList<>();
		sessionTokenMap.entrySet().stream().filter(entry->((SessionToken)entry.getValue()).validator()==false).forEach(entry->list.add(((SessionToken)entry.getValue()).getUserName()));
		list.stream().forEach(str->sessionTokenMap.remove(str));
	}

	public static class SessionToken implements Serializable {
		
		private long startTime;
		private String token;
		private String userName;
		private final static long SESSION_TIME_OUT = 30*60*60*1000;
		public SessionToken(String token,String userName) {
			Date dt= new Date();
			startTime = dt.getTime();
			this.token = token;
			this.userName = userName;
		}

		public SessionToken(long startTime, String token,String userName) {
			this.startTime = startTime;
			this.token = token;
			this.userName = userName;
		}
		
		public String getUserName(){
			return this.userName;
		}
		public boolean validator(){
			Date dt= new Date();
			long endTime = dt.getTime();
			long diff = endTime - this.startTime;
			if(diff < SESSION_TIME_OUT){
				return true;
			}
			return false;
		}

	}

}
