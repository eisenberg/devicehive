package com.devicehive.service;

import static java.util.Collections.singleton;
import static java.util.Optional.empty;
import static java.util.Optional.of;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.devicehive.configuration.Messages;
import com.devicehive.dao.CacheConfig;
import com.devicehive.dao.GenericDAO;
import com.devicehive.exceptions.CommonException;
import com.devicehive.model.AccessKeyPermission;
import com.devicehive.model.Product;
import com.devicehive.model.User;
import com.devicehive.model.enums.UserRole;
import com.devicehive.model.updates.ProductUpdate;
import com.devicehive.service.strategy.LicenceGenerateStrategy;
import com.devicehive.util.HiveValidator;

@Component
public class TenantProductService {

	@Autowired
	private GenericDAO genericDAO;
	@Autowired
	private HiveValidator hiveValidator;
	@Autowired
	private AccessKeyService accessKeyService;
	@Autowired
	private LicenceGenerateStrategy licenceGenerate;
	@Autowired
	private TenantUserService tenantUserService;
	

	@Transactional(propagation = Propagation.REQUIRED)
	public Product ceateProduct(ProductUpdate productUpdate) {
		Optional<String> tenant = productUpdate.getTenant();
		Optional<String> product = productUpdate.getProduct();
		if (!tenant.isPresent() || !product.isPresent()) {
			throw new CommonException("paramete is null", 400);
		}
		User user = tenantUserService.findOrCreateTenantUser(tenant.get());
		return findOrCreateProduct(user, product.get());
	}

	public Product findOrCreateProduct(User user, String productName) {
		productName = StringUtils.trim(productName);
		if (StringUtils.isBlank(productName)) {
			throw new CommonException("product is null", 401);
		}
		Product product = genericDAO.createNamedQuery(Product.class, "Product.findByNameAndUser", empty())
				.setParameter("name", productName).setParameter("login", user.getLogin()).getResultList().stream().findFirst().orElse(null);
		if (null != product) {
			throw new CommonException("product is exist", 402);
		}
		product = new Product();
		product.setName(productName);
		String licence = licenceGenerate.generateLicence(user, productName);
		product.setLicence(licence);
		product.setUser(user);
		hiveValidator.validate(product);
		genericDAO.persist(product);
		return product;
	}
	@Transactional(propagation = Propagation.REQUIRED)
	public Product findTenantProduct(String user,String productName){
		productName = StringUtils.trim(productName);
		user = StringUtils.trim(user);
		if(StringUtils.isBlank(productName) || StringUtils.isBlank(user)){
			throw new CommonException("product is null",404);
		}
		Product product = genericDAO.createNamedQuery(Product.class, "Product.findByNameAndUser", empty())
				.setParameter("name", productName).setParameter("login", user).getResultList().stream().findFirst().orElse(null);
		if(null == product ){
			throw new CommonException("product is empty",404);
		}
		return product;
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public Product findLicenceProduct(String productName,String licence){
		productName = StringUtils.trim(productName);
		licence = StringUtils.trim(licence);
		if(StringUtils.isBlank(productName) || StringUtils.isBlank(licence)){
			return null;
		}
		Product product = genericDAO.createNamedQuery(Product.class, "Product.findByLicenceNameAndUser", empty())
				.setParameter("licence",licence).setParameter("name", productName).getResultList().stream().findFirst().orElse(null);
		
		return product;
	}
	@Transactional(propagation = Propagation.REQUIRED)
	public Product findTenantProduct(String licence){
		licence = StringUtils.trim(licence);
		if(StringUtils.isBlank(licence)){
			return null;
		}
		Product product = genericDAO.createNamedQuery(Product.class, "Product.findProductByLicence", empty())
				.setParameter("licence", licence).getResultList().stream().findFirst().orElse(null);
		
		return product;
	}

	@Transactional
	public boolean authenticateLicence( String licence) {
		if(StringUtils.isBlank(StringUtils.trim(licence))){
		       throw new BadCredentialsException("Invalid credentials");
		}
		long count = genericDAO.createNamedQuery(Long.class, "Product.findByLicence",of(CacheConfig.bypass()))
				.setParameter("licence", licence).getSingleResult();
		return count > 0;
	}	

}
