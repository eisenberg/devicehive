package com.devicehive.service;

import static java.util.Collections.singleton;
import static java.util.Optional.empty;
import static java.util.Optional.of;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.devicehive.configuration.ConfigurationService;
import com.devicehive.configuration.Constants;
import com.devicehive.configuration.Messages;
import com.devicehive.dao.CacheConfig;
import com.devicehive.dao.GenericDAO;
import com.devicehive.exceptions.CommonException;
import com.devicehive.exceptions.IllegalParametersException;
import com.devicehive.model.AccessKey;
import com.devicehive.model.AccessKeyPermission;
import com.devicehive.model.Network;
import com.devicehive.model.User;
import com.devicehive.model.enums.AccessKeyType;
import com.devicehive.model.enums.UserRole;
import com.devicehive.model.enums.UserStatus;
import com.devicehive.model.updates.UserUpdate;
import com.devicehive.service.CacheService.SessionToken;
import com.devicehive.service.helpers.AccessKeyProcessor;
import com.devicehive.service.helpers.PasswordProcessor;
import com.devicehive.service.time.TimestampService;
import com.devicehive.util.HiveValidator;

/**
 * This class serves all requests to database from controller.
 */
@Component
public class TenantUserService {
	private static final Logger logger = LoggerFactory.getLogger(TenantUserService.class);
	

	@Autowired
	private PasswordProcessor passwordService;
	@Autowired
	private GenericDAO genericDAO;
	@Autowired
	private TimestampService timestampService;
	@Autowired
	private ConfigurationService configurationService;
	@Autowired
	private HiveValidator hiveValidator;

	@Autowired
	private CacheService cacheService;
	@Autowired
	private AccessKeyService accessKeyService;

	private PasswordEncoder mPasswordEncoder;
	
	@PostConstruct
	public void init(){
		mPasswordEncoder = new StandardPasswordEncoder();
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public User findOrCreateTenantUser(String name) {
		name = StringUtils.trim(name);
		if (StringUtils.isBlank(name)){
			throw new CommonException("name is null", 403);
		}
		User user = genericDAO.createNamedQuery(User.class, "User.findByName", empty()).setParameter("login", name)
				.getResultList().stream().findFirst().orElse(null);

		if (null == user) {
			user = new User();
			user.setLogin(name);
			user.setRole(UserRole.TENANT);
			user.setLoginAttempts(Constants.INITIAL_LOGIN_ATTEMPTS);
			user.setStatus(UserStatus.ACTIVE);
			hiveValidator.validate(user);
			genericDAO.persist(user);
			createDefaultAccessKeyForUser(user, singleton(new AccessKeyPermission()));
		}
		return user;
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED)
	public User findUser(String name){
		User user = genericDAO.createNamedQuery(User.class, "User.findByName", empty()).setParameter("login", name)
				.getResultList().stream().findFirst().orElse(null);
		if (user == null) {
			throw new CommonException(Messages.USER_NOT_FOUND,401);
		} else if (user.getStatus() != UserStatus.ACTIVE) {
			throw new CommonException(Messages.USER_NOT_ACTIVE,401);
     	} 
		return user;
	}
	
	
	
	@Transactional(propagation = Propagation.REQUIRED)
	public User findWithNetwork(String name) {
		if (StringUtils.isBlank(name))
			throw new CommonException("user invalidae", 400);

		User user = genericDAO.createNamedQuery(User.class, "User.getWithNetworksByLogin", of(CacheConfig.bypass()))
				.setParameter("login", name).getResultList().stream().findFirst().orElse(null);
		return user;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public User createUser(@NotNull User user, String password) {
		if (user.getId() != null) {
			throw new IllegalParametersException(Messages.ID_NOT_ALLOWED);
		}
		final String userLogin = StringUtils.trim(user.getLogin());
		user.setLogin(userLogin);
		Optional<User> existing = genericDAO.createNamedQuery(User.class, "User.findByName", of(CacheConfig.bypass()))
				.setParameter("login", user.getLogin()).getResultList().stream().findFirst();
		if (existing.isPresent()) {
			throw new CommonException("the name has been register",401);
		}
		if (StringUtils.isNoneEmpty(password)) {
			//String salt = passwordService.generateSalt();
			//String hash = passwordService.hashPassword(password, salt);
			//user.setPasswordSalt(salt);
			String hash = mPasswordEncoder.encode(password);
			user.setPasswordHash(hash);
		}
		user.setLoginAttempts(Constants.INITIAL_LOGIN_ATTEMPTS);
		user.setRole(UserRole.TENANT);
//		String verifyHash = RandomStringUtils.randomAlphanumeric(Constants.PASSWORD_RESET_HASH_LENGTH);
//		String tenantToken = RandomStringUtils.randomAlphanumeric(Constants.APP_TOKEN_SIZE);
//		String tenantKey = passwordService.generateSalt();
//		StringBuilder sb = new StringBuilder();
//		sb.append(UUID.randomUUID().toString()).append("|").append(userLogin);
//		String licence = mPasswordEncoder.encode(sb.toString());
//		user.setLicence(licence);
		hiveValidator.validate(user);
		genericDAO.persist(user);
		// create accesskey
		createDefaultAccessKeyForUser(user,null);
		// create network
		createDefaultNetworkForUser(user);
		return user;
	}

	public void createDefaultAccessKeyForUser(User user, Set<AccessKeyPermission> permissions) {
		AccessKey accessKey = new AccessKey();
		if (permissions == null) {
			permissions = Collections.singleton(new AccessKeyPermission());
		}
		accessKey.setPermissions(permissions);
		accessKey.setKtype(AccessKeyType.DEFAULT);
		accessKey.setLabel(Constants.DEFAULT_ACCESSKEY_LABEL);
		accessKeyService.create(user, accessKey);
	}
	
	public Network createDefaultNetworkForUser(User user){
		Network newNetwork = new Network();
		String name = user.getLogin();
		StringBuilder sb = new StringBuilder();
		sb.append(name).append(Constants.DEFAULT_NETWORK_NAME);
		newNetwork.setName(sb.toString());
		AccessKeyProcessor keyProcessor = new AccessKeyProcessor();
		newNetwork.setNkey(keyProcessor.generateKey());
		newNetwork.setDescription(Constants.DEFAULT_NETWORK_LABEL);
		
		Set<User>  usersSet = new HashSet<User>();
		usersSet.add(user);
		newNetwork.setUsers(usersSet);
		genericDAO.merge(newNetwork);
		return newNetwork;
	}
	
	

	//@Transactional(noRollbackFor = AccessDeniedException.class)
	public User findUser(String login, String password) {
		User user = genericDAO.createNamedQuery(User.class, "User.findByName", empty()).setParameter("login", login)
				.getResultList().stream().findFirst().orElse(null);
		if (user == null) {
			logger.error("Can't find user with login {} and password {}", login, password);
			throw new CommonException("donn't exist user ",403);
		} else if (user.getStatus() != UserStatus.ACTIVE) {
			logger.error("User with login {} is not active", login);
			throw new CommonException("use isn't active",404);
		} 
		logger.error("use is {}", user.toString());
		return checkPassword(user, password)
				.orElseThrow(() -> new CommonException("error password", 405));
	}

	private Optional<User> checkPassword(User user, String password) {
		boolean validPassword = mPasswordEncoder.matches(password, user.getPasswordHash());
		if(!validPassword)throw new CommonException(password,4000);
		long loginTimeout = configurationService.getLong(Constants.LAST_LOGIN_TIMEOUT,
				Constants.LAST_LOGIN_TIMEOUT_DEFAULT);
		boolean mustUpdateLoginStatistic = user.getLoginAttempts() != 0 || user.getLastLogin() == null
				|| System.currentTimeMillis() - user.getLastLogin().getTime() > loginTimeout;

		if (validPassword && mustUpdateLoginStatistic) {
			return of(updateStatisticOnSuccessfulLogin(user, loginTimeout));
		} else if (!validPassword) {
			user.setLoginAttempts(user.getLoginAttempts() + 1);
			if (user.getLoginAttempts() >= configurationService.getInt(Constants.MAX_LOGIN_ATTEMPTS,
					Constants.MAX_LOGIN_ATTEMPTS_DEFAULT)) {
				user.setStatus(UserStatus.LOCKED_OUT);
				user.setLoginAttempts(0);
			}
			genericDAO.merge(user);
			return empty();
		}
		return of(user);
	}

	private User updateStatisticOnSuccessfulLogin(User user, long loginTimeout) {
		boolean update = false;
		if (user.getLoginAttempts() != 0) {
			update = true;
			user.setLoginAttempts(0);
		}
		if (user.getLastLogin() == null || System.currentTimeMillis() - user.getLastLogin().getTime() > loginTimeout) {
			update = true;
			user.setLastLogin(timestampService.getTimestamp());
		}
		return update ? genericDAO.merge(user) : user;
	}
	@Transactional(propagation = Propagation.REQUIRED)
	public User authenticate(String name, String password) {
		SessionToken st = (SessionToken)cacheService.get(CacheService.SESSION_TOKEN_MAP,name);
		if(null !=st && st.validator()){
			throw new CommonException("login repeat",402);
		}
		User user = findUser(name, password);
		StringBuilder sb = new StringBuilder();
		sb.append(UUID.randomUUID().toString().toUpperCase()).append("|").append(name).append("|").append(getCurrentTime());
		String key = sb.toString();
//		StandardPBEStringEncryptor jasypt = new StandardPBEStringEncryptor();
		// this is the authentication token user will send in order to use the
		// web service
//		jasypt.setPassword(Constants.TOKEN);
//		String authenticationToken = jasypt.encrypt(key);
		//cacheService.put(key, authenticationToken);//TODO need to cache token in time ;
//		SessionToken stk = new SessionToken(authenticationToken,name);
//		cacheService.put(CacheService.SESSION_TOKEN_MAP, name, stk);
//		user.setSessionToken(authenticationToken);
		return user;
	}

	private final static String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

	private String getCurrentTime() {
		Calendar cal = Calendar.getInstance();
		String currentDate = DateFormatUtils.format(cal, TIME_FORMAT);
		cal = null;
		return currentDate;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void logout(String username){
		cacheService.remove(CacheService.SESSION_TOKEN_MAP, username);
	}
	@Transactional(propagation = Propagation.REQUIRED)
	public User updateUser(UserUpdate userUpdate){
		String username = StringUtils.trim(userUpdate.getLogin().getValue());
		String oldPassword = StringUtils.trim(userUpdate.getOldPassword().getValue());
		String password = StringUtils.trim(userUpdate.getPassword().getValue());
		
		User user = findUser(username,oldPassword);
		String hash = mPasswordEncoder.encode(password);
		user.setPasswordHash(hash);
		genericDAO.merge(user);
		return user;
	}
	
}
