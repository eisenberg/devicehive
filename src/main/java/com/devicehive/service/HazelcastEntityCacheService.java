package com.devicehive.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devicehive.model.DeviceCommand;
import com.devicehive.model.DeviceNotification;
import com.devicehive.model.HazelcastEntity;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

@Service
public class HazelcastEntityCacheService {
    @Autowired
    protected HazelcastInstance hazelcastInstance;
	private Map<Class, IMap<String, Object>> mapsHolder;

	@PostConstruct
	protected void init() {
		final IMap<String, Object> notificationsMap = hazelcastInstance.getMap("NOTIFICATIONS-MAP");
		final IMap<String, Object> commandsMap = hazelcastInstance.getMap("COMMANDS-MAP");

		mapsHolder = new HashMap<>(2);
		mapsHolder.put(DeviceNotification.class, notificationsMap);
		mapsHolder.put(DeviceCommand.class, commandsMap);
	}
	
	public <T extends HazelcastEntity> IMap<String, Object> get(Class<T> clazz){
		return mapsHolder.get(clazz);
	}
}
