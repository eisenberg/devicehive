package com.devicehive.service;

import static java.util.Optional.of;
import static java.util.Optional.ofNullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.devicehive.auth.HivePrincipal;
import com.devicehive.dao.CacheConfig;
import com.devicehive.dao.CriteriaHelper;
import com.devicehive.dao.DeviceCriteriaHelper;
import com.devicehive.dao.GenericDAO;
import com.devicehive.exceptions.CommonException;
import com.devicehive.json.tenant.model.DeviceCriteriaPropertyConstants;
import com.devicehive.model.Device;
import com.devicehive.model.DeviceClass;
import com.devicehive.model.Product;
import com.devicehive.model.User;
import com.devicehive.model.tenant.TenantDevice;
import com.devicehive.model.tenant.TenantDeviceList;
import com.devicehive.service.strategy.DeviceNameGenerate;
import com.google.gson.JsonObject;

@Component
public class TenantDeviceService {
	@Autowired
	private GenericDAO genericDAO;
	@Autowired
	private TenantProductService tenantProductService;
	@Autowired
	private DeviceNameGenerate deviceNameGenerate;
	@Autowired
	private DeviceClassService deviceClassService;

	@Transactional(propagation = Propagation.REQUIRED)
	public List<Device> batchInsertDevices(TenantDeviceList deviceList) {
		String productName = StringUtils.trim(deviceList.getProduct());
		String tenant = StringUtils.trim(deviceList.getTenant());
		TenantDevice[] tenantDevice = deviceList.getDevices();
		if (StringUtils.isBlank(tenant) || StringUtils.isBlank(productName) || null == tenantDevice) {
			throw new CommonException("product or  tenant is null", 400);
		}
		Product product = tenantProductService.findTenantProduct(tenant, productName);
		DeviceClass deviceClass = deviceClassService.findDefaultDeviceClass();

		List<TenantDevice> devices = Arrays.asList(tenantDevice);
		final List<Device> devicesInsert = new ArrayList<Device>();
		devices.stream().forEach(tdevice -> {
			// Device device = new Device();
			// device.setGuid(tdevice.getDeviceID());
			// device.setMac(tdevice.getMac());
			Device device = tdevice.convertTo();
			if (null != device) {
				device.setName(deviceNameGenerate.generateName(deviceList, tdevice));
				device.setBlocked(false);
				device.setStatus("Offline");
				device.setProduct(product);
//				if(!genericDAO.contain(deviceClass) && null != deviceClass.getId()){
//					device.setDeviceClass(genericDAO.merge(deviceClass));
//				}else{
				device.setDeviceClass(deviceClass);
//				}
				devicesInsert.add(device);
			}

		});

		final List<Device> savedEntities = new ArrayList<Device>(devicesInsert.size());
		genericDAO.bulkSave(devicesInsert, savedEntities);
		devicesInsert.removeAll(savedEntities);
		return devicesInsert;

	}

	@Transactional(readOnly = true)
	public List<Device> getBatchDevice(Integer numonly, Integer take, Integer skip, JsonObject object,
			HivePrincipal principal) {
		final CriteriaBuilder cb = genericDAO.criteriaBuilder();
		final CriteriaQuery<Device> criteria = cb.createQuery(Device.class);
		final Root<Device> from = criteria.from(Device.class);

		return null;

	}

	@Transactional(readOnly = true)
	public List<Device> getList(JsonObject jsonObj, Integer take, Integer skip) {
		List<Device> list = new ArrayList<Device>();
		final CriteriaBuilder cb = genericDAO.criteriaBuilder();
		final CriteriaQuery<Device> criteria = cb.createQuery(Device.class);
		final Root<Device> from = criteria.from(Device.class);

		String tenant = jsonObj.get(DeviceCriteriaPropertyConstants.TENANT).getAsString();
		String product = jsonObj.get(DeviceCriteriaPropertyConstants.PRODUCT).getAsString();
		final Predicate[] predicates = DeviceCriteriaHelper.deviceListPredicates(cb, from, Optional.empty(),
				Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(),
				Optional.ofNullable(tenant), Optional.ofNullable(product));

		criteria.where(predicates);
		// CriteriaHelper.order(cb, criteria, from, ofNullable(sortField),
		// sortOrderAsc);

		final TypedQuery<Device> query = genericDAO.createQuery(criteria);
		genericDAO.cacheQuery(query, of(CacheConfig.refresh()));
		ofNullable(take).ifPresent(query::setMaxResults);
		ofNullable(skip).ifPresent(query::setFirstResult);
		
		if(null != query.getResultList() )
		  list.addAll(query.getResultList());
		return list;
	}

	//
	// private Network findNetwork(User user, TenantDevice deviceUpdate) {
	// Set<Network> networks = user.getNetworks();
	// Network returnNetwork = null;
	// if (null == networks || networks.size() == 0)
	// throw new CommonException("user's network is null", 403);
	// if (StringUtils.isBlank(deviceUpdate.getNetworkId())) {
	// returnNetwork = networks.stream()
	// .filter(network ->
	// network.getDescription().equals(Constants.DEFAULT_NETWORK_LABEL)).findFirst()
	// .orElse(null);
	// } else {
	// String networkKey = deviceUpdate.getNetworkId();
	// returnNetwork = networks.stream().filter(network ->
	// network.getNkey().equals(networkKey)).findFirst()
	// .orElse(null);
	// }
	// if (null == returnNetwork)
	// throw new CommonException("no network", 404);
	//
	// return returnNetwork;
	// }
	//
	// @Transactional(propagation = Propagation.REQUIRED)
	// public void deviceSaveByUser(TenantDevice deviceUpdate, String name) {
	// Device existingDevice = genericDAO
	// .createNamedQuery(Device.class, "Device.findNetworkByVendorId",
	// Optional.of(CacheConfig.refresh()))
	// .setParameter("vendordeviceid",
	// deviceUpdate.getDeviceID()).getResultList().stream().findFirst()
	// .orElse(null);
	//
	// if (StringUtils.isBlank(name))
	// throw new CommonException("user invalidate", 401);
	//
	// // 重复添加
	// // 已经绑定
	// // TODO
	//
	// User user = tenantUserService.findWithNetwork(StringUtils.trim(name));
	// if (null == user)
	// throw new CommonException("user invalidate", 401);
	//
	// if (existingDevice == null) {
	// Device device = deviceUpdate.convertTo();
	//
	// Network network = findNetwork(user, deviceUpdate);
	//
	// if (network != null) {
	// device.setNetwork(network);
	// }
	// if (device.getBlocked() == null) {
	// device.setBlocked(false);
	// }
	// genericDAO.persist(device);
	// } else {
	// if (!userService.hasAccessToDevice(user, existingDevice.getGuid())) {
	// throw new HiveException(Messages.UNAUTHORIZED_REASON_PHRASE,
	// UNAUTHORIZED.getStatusCode());
	// } else {
	// throw new CommonException("add repeat", 402);
	// }
	// // if (deviceUpdate.getStatus() != null) {
	// // existingDevice.setStatus(deviceUpdate.getStatus().getValue());
	// // }
	// // if (deviceUpdate.getData() != null) {
	// //
	// existingDevice.setData(deviceUpdate.getData().getValue().getJsonString());
	// // }
	// // if (deviceUpdate.getNetwork() != null) {
	// // existingDevice.setNetwork(network);
	// // }
	// // if (deviceUpdate.getName() != null) {
	// // existingDevice.setName(deviceUpdate.getName().getValue());
	// // }
	// // if (deviceUpdate.getKey() != null) {
	// // existingDevice.setDkey(deviceUpdate.getKey().getValue());
	// // }
	// // if (deviceUpdate.getBlocked() != null) {
	// // existingDevice.setBlocked(deviceUpdate.getBlocked().getValue());
	// // }
	//
	// }
	// }
	//
	// public boolean deleteDevice(String guid) {
	//
	// return true;
	// }

}
