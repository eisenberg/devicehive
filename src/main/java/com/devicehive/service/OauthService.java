package com.devicehive.service;

import static java.util.Optional.of;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.devicehive.configuration.Constants;
import com.devicehive.configuration.Messages;
import com.devicehive.dao.CacheConfig;
import com.devicehive.dao.GenericDAO;
import com.devicehive.exceptions.ActionNotAllowedException;
import com.devicehive.exceptions.IllegalParametersException;
import com.devicehive.exceptions.CommonException;
import com.devicehive.model.AccessKey;
import com.devicehive.model.AccessKeyPermission;
import com.devicehive.model.Device;
import com.devicehive.model.Network;
import com.devicehive.model.User;
import com.devicehive.model.enums.UserRole;
import com.devicehive.model.updates.UserUpdate;
import com.devicehive.service.helpers.AccessKeyProcessor;
import com.devicehive.service.helpers.PasswordProcessor;
import com.devicehive.util.HiveValidator;

/**
 * 
 * @author nathan
 * 
 */
@Component
public class OauthService {
	private static final Logger logger = LoggerFactory.getLogger(OauthService.class);
	@Autowired
	private GenericDAO genericDAO;
	@Autowired
	private HiveValidator hiveValidator;
	@Autowired
	private PasswordProcessor passwordService;

	@Transactional(propagation = Propagation.REQUIRED)
	public Device findDevice(String vendordeviceid, String weixindeviceid, UserUpdate userUpdate) {
		vendordeviceid = vendordeviceid.trim();
		if (StringUtils.isBlank(vendordeviceid)) {
			throw new CommonException("vendordevice invalidate", 401);
		}
		if (null != weixindeviceid)
			weixindeviceid = weixindeviceid.trim();

		Optional<Device> deviceExister = genericDAO
				.createNamedQuery(Device.class, "Device.findNetworkByVendorId", Optional.of(CacheConfig.bypass()))
				.setParameter("vendordeviceid", vendordeviceid).getResultList().stream().findFirst();
		if (!deviceExister.isPresent()) {
			throw new CommonException("no device" + vendordeviceid, 401);
		}
		Device device = deviceExister.get();
		Network network = device.getNetwork();
		if (network != null && null != network.getId()) {
			network = genericDAO
					.createNamedQuery(Network.class, "Network.findWithUsers", Optional.of(CacheConfig.refresh()))
					.setParameter("id", network.getId()).getResultList().stream().findFirst().orElse(null);

			Set<User> users = network.getUsers();
			if (null != users) {
				for (Iterator<User> iterator = users.iterator(); iterator.hasNext();) {
					User user = (User) iterator.next();
					if (userUpdate.getLogin().getValue().equals(user.getLogin())) {
						throw new CommonException("bind repeated", 403);
					}
				}
				if (users.size() > 0) {
					throw new CommonException("device has binded", 402);
				}
			}

		}
		/*null == weixindeviceid || "".equals(weixindeviceid.trim())*/
		if (StringUtils.isBlank(StringUtils.trim(weixindeviceid))) {
			if (null == device.getWeixindeviceid()) {
				/*null == device.getMac() || "".equals(device.getMac().trim())*/
				if (StringUtils.isBlank(StringUtils.trim(device.getMac()))) {
					throw new CommonException("no mac", 404);
				}
				throw new CommonException(device.getMac().trim(), 400);
			}
		} else if (StringUtils.isBlank(StringUtils.trim((device.getWeixindeviceid())))) {
			device.setWeixindeviceid(weixindeviceid.trim());
			genericDAO.merge(device);
		}

		return device;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public User findUser(UserUpdate userUpdate) {
		User user = userUpdate.convertTo();
		if (StringUtils.isBlank(StringUtils.trim(user.getLogin()))) {
			//throw new IllegalParametersException(Messages.ID_NOT_ALLOWED);
			throw new CommonException("user info error", 405);
		}
		String login = user.getLogin().trim();
		Optional<User> existing = genericDAO
				.createNamedQuery(User.class, "User.getWithNetworksByLogin", of(CacheConfig.bypass()))
				.setParameter("login", login).getResultList().stream().findFirst();

		if (existing.isPresent()) {
			return existing.get();
		} else {
			String password = userUpdate.getPassword() == null ? null : userUpdate.getPassword().getValue();
			final String userLogin = StringUtils.trim(user.getLogin());
			user.setLogin(userLogin);

			if (StringUtils.isNoneEmpty(password)) {
				String salt = passwordService.generateSalt();
				String hash = passwordService.hashPassword(password, salt);
				user.setPasswordSalt(salt);
				user.setPasswordHash(hash);
			}
            user.setRole(UserRole.OAUTH);
			user.setLoginAttempts(Constants.INITIAL_LOGIN_ATTEMPTS);

			hiveValidator.validate(user);
			genericDAO.persist(user);

			return user;
		}
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void unBindDevice(@NotNull String vendordeviceid, @NotNull String openid) {
		if (StringUtils.isBlank(vendordeviceid)|| StringUtils.isBlank(openid)) {
			throw new CommonException("param invalidate", 400);
		}
		String vid = vendordeviceid.trim();
		String userName = openid.trim();

		Optional<User> existingUser = genericDAO
				.createNamedQuery(User.class, "User.getWithNetworksByLogin", of(CacheConfig.bypass()))
				.setParameter("login", userName).getResultList().stream().findFirst();
		if (!existingUser.isPresent() || null == existingUser.get().getNetworks()) {
			throw new CommonException("openidinvalidate", 401);
		}
		Set<Network> set = existingUser.get().getNetworks();

		Device existingDevice = genericDAO
				.createNamedQuery(Device.class, "Device.findNetworkByVendorId", Optional.of(CacheConfig.refresh()))
				.setParameter("vendordeviceid", vid).getResultList().stream().findFirst().orElse(null);

		if (null == existingDevice || null == existingDevice.getNetwork()) {
			throw new CommonException("vendordeviceid invalidate", 400);
		}

		if (set.contains(existingDevice.getNetwork())) {
			existingDevice.setNetwork(null);
			genericDAO.merge(existingDevice);
		} else {
			throw new CommonException("device unbind user", 402);
		}

	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void bindByUser(Device device, User user, int networkId) {
		// 用户存在了network
		Network network = null;
		if (null != user.getNetworks() && user.getNetworks().size() > 0) {
			network = getNetwork(user.getNetworks(), networkId);
			if (null == network) {
				network = saveNetwork(user);
			}
		} // 用户不存在network,需要创建network
		else {
			network = saveNetwork(user);
		}
		
		// 最終是綁定device和user;即device和network
		if (network != null) {
			device.setNetwork(network);
		}
		genericDAO.merge(device);
	}

	public AccessKey getAccessKey(String login) {
		return genericDAO
				.createNamedQuery(AccessKey.class, "AccessKey.getByUserLogin", Optional.of(CacheConfig.refresh()))
				.setParameter("login", login).getResultList().stream().findFirst().orElse(null);
	}
	
	private Network saveNetwork(User user) {
		Network createNetwork = createNetwork(Constants.DEFAULT_NETWORK_NAME);
		Set<User> usersSet = createNetwork.getUsers();
		if (null == usersSet) {
			usersSet = new HashSet<User>();
		}
		usersSet.add(user);
		createNetwork.setUsers(usersSet);
		genericDAO.merge(createNetwork);
		return createNetwork;
	}

	public Network createNetwork(String name) {
		Network newNetwork = new Network();
		if(StringUtils.isBlank(StringUtils.trim(name))){
			StringBuilder sb = new StringBuilder();
			sb.append(Constants.DEFAULT_NETWORK_NAME);
			name = sb.toString();
		}
		newNetwork.setName(name);
		AccessKeyProcessor keyProcessor = new AccessKeyProcessor();
		newNetwork.setNkey(keyProcessor.generateKey());
		newNetwork.setDescription(Constants.DEFAULT_NETWORK_LABEL);

		genericDAO.persist(newNetwork);
		logger.info("Entity {} created successfully", newNetwork);
		return newNetwork;
	}

	private Network getNetwork(@NotNull Set<Network> networks, @NotNull int networkId) {
		for (Iterator iterator = networks.iterator(); iterator.hasNext();) {
			Network network = (Network) iterator.next();
			if (networkId == network.getId())
				return network;

		}
		return networks.stream().findFirst().orElse(null);
	}
	
	public boolean accessDevice(String vendordeviceid,String openid){
		Optional<Device> deviceExister = genericDAO
				.createNamedQuery(Device.class, "Device.findNetworkByVendorId", Optional.of(CacheConfig.bypass()))
				.setParameter("vendordeviceid", vendordeviceid).getResultList().stream().findFirst();
		if (!deviceExister.isPresent()) {
			throw new CommonException("vendordeviceid invalidate:" + vendordeviceid, 400);
		}
		
		Optional<User> existingUser = genericDAO
				.createNamedQuery(User.class, "User.getWithNetworksByLogin", of(CacheConfig.bypass()))
				.setParameter("login", openid).getResultList().stream().findFirst();
		if (!existingUser.isPresent() || null == existingUser.get().getNetworks()) {
			throw new CommonException("openid invalidate", 401);
		}
		Set<Network> set = existingUser.get().getNetworks();
		
		Network network = deviceExister.get().getNetwork();
		if(null == network)
			return true;
		if(null == set || !set.contains(network)){
			throw new CommonException("can not access device", 402);
		}
		return true;
	}

}
