package com.devicehive.exceptions;

public class CommonException extends RuntimeException {
    private int code;

	public CommonException(String message,int code) {
		   super(message);
		   this.code = code;
	}
	
	public int getCode(){
		return code;
	}

}
