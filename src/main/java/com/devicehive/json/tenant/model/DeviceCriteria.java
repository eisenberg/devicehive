package com.devicehive.json.tenant.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.thymeleaf.util.StringUtils;

import com.devicehive.json.tenant.model.annotation.CriteriaParam;
import com.devicehive.json.tenant.model.annotation.DeviceCriteriaPredicate;
import com.devicehive.json.tenant.model.annotation.DeviceCriteriaProperty;
import com.devicehive.model.Device;
import com.devicehive.model.Network;
import com.devicehive.model.Product;

public class DeviceCriteria {

	@DeviceCriteriaProperty
	public final String TENANT = DeviceCriteriaPropertyConstants.TENANT;
	@DeviceCriteriaProperty
	public final String PRODUCT = DeviceCriteriaPropertyConstants.PRODUCT;
	
	public final String TENANT_PRODUCT="tenant_product";

	@DeviceCriteriaPredicate(property = TENANT_PRODUCT)
	public void generateTenantPredicate(CriteriaBuilder cb, Root<Device> from, CriteriaQuery<Device> criteria,
			@CriteriaParam(DeviceCriteriaPropertyConstants.TENANT)
			Optional<String> tenant,
			@CriteriaParam(DeviceCriteriaPropertyConstants.PRODUCT)
			Optional<String> product,
			List<Predicate> predicates) {
		tenant.ifPresent(t ->{
			product.ifPresent(p ->{
				final Join<Device, Product> productJoin = (Join) from.fetch("product", JoinType.LEFT);
				final Join<Device, Network> usersJoin = (Join) productJoin.fetch("user", JoinType.LEFT);
				predicates.add(cb.equal(productJoin.<Long>get("name"),StringUtils.trim(p)));
				predicates.add(cb.equal(usersJoin.<String>get("login"),StringUtils.trim(t)));
			});
			
		});
		
		
		
	}

}
