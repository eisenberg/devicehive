package com.devicehive.json.tenant.model;

import com.devicehive.json.tenant.model.annotation.DeviceCriteriaProperty;

public class DeviceCriteriaPropertyConstants {

	public static final int DEFAULT_TAKE = 100;
	public static final String TENANT = "tenant";
	public static final String PRODUCT = "product";
}
