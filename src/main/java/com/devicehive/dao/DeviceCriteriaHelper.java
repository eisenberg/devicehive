package com.devicehive.dao;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.socket.WebSocketSession;

import com.devicehive.auth.HivePrincipal;
import com.devicehive.json.GsonFactory;
import com.devicehive.json.tenant.model.DeviceCriteria;
import com.devicehive.json.tenant.model.annotation.CriteriaParam;
import com.devicehive.json.tenant.model.annotation.DeviceCriteriaPredicate;
import com.devicehive.json.tenant.model.annotation.DeviceCriteriaProperty;
import com.devicehive.model.Device;
import com.devicehive.model.DeviceClass;
import com.devicehive.model.Network;
import com.devicehive.model.Product;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.JsonObject;


public class DeviceCriteriaHelper {
	private DeviceCriteria deviceCriteria;
//	private List<String> propertyCache;
	private Map<String,Type> propertyCache;
	private Map<String, Map<String, Method>> methodCache;
	private Map<Method, List<ParameterDescriptor>> parametersCache;

	@PostConstruct
	public void init() {
		deviceCriteria = new DeviceCriteria();
		propertyCache = new HashMap<>();
		methodCache = new HashMap<>();

		ReflectionUtils.doWithMethods(deviceCriteria.getClass(), m -> {
			ReflectionUtils.makeAccessible(m);
			Preconditions.checkArgument(m.getReturnType().equals(Void.class), "Method should have %s return type",
					Void.class.getName());
			DeviceCriteriaPredicate deviceCriteriaPredicate = m.getAnnotation(DeviceCriteriaPredicate.class);
			String property = deviceCriteriaPredicate.property();
			String action = deviceCriteriaPredicate.action();
			Map<String, Method> methodItemMap = methodCache.get(property);
			if (null == methodItemMap) {
				methodItemMap = new HashMap<String, Method>();
				methodCache.put(property, methodItemMap);
			}
			methodItemMap.put(action, m);

			Type[] parameterTypes = m.getGenericParameterTypes();
			Annotation[][] allAnnotations = m.getParameterAnnotations();

			List<ParameterDescriptor> descriptors = new ArrayList<>(parameterTypes.length);
			for (int i = 0; i < parameterTypes.length; i++) {
				Type type = parameterTypes[i];
				String name = null;
				for (Annotation currentParamAnnotation : allAnnotations[i]) {
					if (currentParamAnnotation instanceof CriteriaParam) {
						name = ((CriteriaParam) currentParamAnnotation).value();
					}
				}
				descriptors.add(new ParameterDescriptor(name, type));
			}
			parametersCache.put(m, descriptors);

		} , m -> m.isAnnotationPresent(DeviceCriteriaPredicate.class));

		ReflectionUtils.doWithFields(deviceCriteria.getClass(), f -> {
			ReflectionUtils.makeAccessible(f);
			Preconditions.checkArgument(f.getType().equals(String.class), "Method should have %s return type",
					String.class.getName());
//			propertyCache.add((String) f.get(deviceCriteria));
			propertyCache.put((String) f.get(deviceCriteria), f.getType());
		} , f -> f.isAnnotationPresent(DeviceCriteriaProperty.class));

	}

	public Predicate[] deviceListDefaultPredicates(CriteriaBuilder cb, Root<Device> from, CriteriaQuery<Device> criteria,
			JsonObject jsonObj, Optional<HivePrincipal> principal) {
		final List<Predicate> predicates = new LinkedList<>();
		propertyCache.keySet().forEach(property ->{
			//TODO need to consider not string condition
			String value = jsonObj.get(property).getAsString();
			value = StringUtils.trim(value);
			if(StringUtils.isNotBlank(value)){
				Map<String,Method>  methodItemCache = methodCache.get(property);
				Method method = methodItemCache.get("default");
				List<ParameterDescriptor> descriptors = parametersCache.get(method);
				
//				 List<WebsocketParameterDescriptor> descriptors = getArguments(executedMethod);
			        List<Object> values = new ArrayList<>(descriptors.size());

			        for (ParameterDescriptor descriptor : descriptors) {
			            Type type = descriptor.getType();
			            if (CriteriaBuilder.class.equals(type)) {
			                values.add(cb);
			            } else if(Root.class.equals(type)){
			            	 values.add(from);
			            } else if(CriteriaQuery.class.equals(type)){
			            	 values.add(criteria);
			            }
			        }
				
			}
		});
	
		   return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	
    @SuppressWarnings("unchecked")
    public static Predicate[] deviceListPredicates(CriteriaBuilder cb,
                                                   Root<Device> from,
                                                   Optional<String> name,
                                                   Optional<String> namePattern,
                                                   Optional<String> status,
                                                   Optional<Long> deviceClassId,
                                                   Optional<String> deviceClassName,
                                                   Optional<String> deviceClassVersion,
                                                   Optional<String> tenant,
                                                   Optional<String> product) {
        final List<Predicate> predicates = new LinkedList<>();

        name.ifPresent(n -> predicates.add(cb.equal(from.<String>get("name"), n)));
        namePattern.ifPresent(np -> predicates.add(cb.like(from.<String>get("name"), np)));
        status.ifPresent(s -> predicates.add(cb.equal(from.<String>get("status"), s)));

        final Join<Device, DeviceClass> dcJoin = (Join) from.fetch("deviceClass", JoinType.LEFT);
        deviceClassId.ifPresent(dcId -> predicates.add(cb.equal(dcJoin.<Long>get("id"), dcId)));
        deviceClassName.ifPresent(dcName -> predicates.add(cb.equal(dcJoin.<String>get("name"), dcName)));
        deviceClassVersion.ifPresent(dcVersion -> predicates.add(cb.equal(dcJoin.<String>get("version"), dcVersion)));

        tenant.ifPresent(t ->{
			product.ifPresent(p ->{
				final Join<Device, Product> productJoin = (Join) from.fetch("product", JoinType.LEFT);
				final Join<Device, Product> usersJoin = (Join) productJoin.fetch("user", JoinType.LEFT);
				predicates.add(cb.equal(productJoin.<Long>get("name"),StringUtils.trim(p)));
				predicates.add(cb.equal(usersJoin.<String>get("login"),StringUtils.trim(t)));
			});
			
		});

        return predicates.toArray(new Predicate[predicates.size()]);
    }

	private static class ParameterDescriptor {

		private String name;
		private Type type;

		public ParameterDescriptor(String name, Type type) {
			this.name = name;
			this.type = type;
		}

		private String getName() {
			return name;
		}

		private Type getType() {
			return type;
		}
	}

}
